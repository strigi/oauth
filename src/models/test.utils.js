import { expect, it } from '@jest/globals'

export function mutablePropertyTests (Model, propertyName, { value } = {}) {
  value = value || `my test ${propertyName}`

  it(`can be validly initialized with '${value}'`, () => {
    const model = new Model({ [propertyName]: value })
    expect(model.validateSync(propertyName)).toBeUndefined()

    if (value.toObject) {
      expect(model[propertyName].toObject()).toEqual(value.toObject())
    } else {
      expect(model[propertyName]).toEqual(value)
    }
  })

  it(`can be validly set with '${value}'`, () => {
    const model = new Model()
    model[propertyName] = value
    expect(model.validateSync(propertyName)).toBeUndefined()

    if (value.toObject) {
      expect(model[propertyName].toObject()).toEqual(value.toObject())
    } else {
      expect(model[propertyName]).toEqual(value)
    }
  })

  // TODO: this is maybe just stupid or should be it's own tests function
  // it('casts well known and primitives type to string', () => {
  //   expect(new Model({ [propertyName]: 1507 })[propertyName]).toBe('1507')
  //   expect(new Model({ [propertyName]: false })[propertyName]).toBe('false')
  //
  //   const model = new Model()
  //   model[propertyName] = 1507
  //   expect(model[propertyName]).toBe('1507')
  // })
}

export function defaultPropertyTests (Model, propertyName, defaultValue) {
  it(`has default value '${defaultValue}' when not initialized`, () => {
    const value = new Model({})[propertyName]
    if (defaultValue) {
      expect(value).toEqual(defaultValue)
    } else {
      expect(value).toBeDefined()
    }
  })

  it('has default value when initialized to undefined', () => {
    const value = new Model({ [propertyName]: undefined })[propertyName]
    if (defaultValue) {
      expect(value).toEqual(defaultValue)
    } else {
      expect(value).toBeDefined()
    }
  })
}

export function requiredPropertyTests (Model, propertyName, { includeUndefined = true } = {}) {
  // Undefined is considered a candidate for default values, so properties that have a default value
  // usually want to not include the setting of undefined to be expecting a validation error (since the default value
  // will take over).
  if (includeUndefined === false) {
    it('rejects validation of omitted', () => {
      const model = new Model()
      expect(model.validateSync(propertyName)?.message).toBe(requiredErrorMessage(Model, propertyName))
    })

    it('rejects validation if initialized to undefined', () => {
      const model = new Model({ [propertyName]: undefined })
      expect(model.validateSync(propertyName)?.message).toBe(requiredErrorMessage(Model, propertyName))
    })
  }

  it('rejects validation if set to undefined', () => {
    const model = new Model()
    model[propertyName] = undefined
    expect(model.validateSync(propertyName)?.message).toBe(requiredErrorMessage(Model, propertyName))
  })

  it('rejects validation if initialized to null', () => {
    expect(new Model({ [propertyName]: null }).validateSync(propertyName)?.message).toBe(requiredErrorMessage(Model, propertyName))
  })

  it('rejects validation if set to null', () => {
    const model = new Model()
    model[propertyName] = null
    expect(model.validateSync(propertyName)?.message).toBe(requiredErrorMessage(Model, propertyName))
  })
}

export function nonBlankStringPropertyTests (Model, propertyName) {
  it('rejects validation if initialized to empty string', () => {
    expect(new Model({ [propertyName]: '' }).validateSync(propertyName)?.message).toBe(requiredErrorMessage(Model, propertyName))
  })

  it('rejects validation if initialized to blank string', () => {
    expect(new Model({ [propertyName]: ' \t \n ' }).validateSync(propertyName)?.message).toBe(requiredErrorMessage(Model, propertyName))
  })

  it('rejects validation if set to blank string', () => {
    const empty = new Model()
    empty[propertyName] = ''
    expect(empty.validateSync(propertyName)?.message).toBe(requiredErrorMessage(Model, propertyName))
  })

  it('rejects validation if set to empty string', () => {
    const blank = new Model()
    blank[propertyName] = ' \t \n '
    expect(blank.validateSync(propertyName)?.message).toBe(requiredErrorMessage(Model, propertyName))
  })
}

export function arrayPropertyTests (Model, propertyName, { emptyAllowed = false } = {}) {
  it('must not be null', async () => {
    expect(new Model({ [propertyName]: null }).validateSync(propertyName)).toHaveProperty('message', `${Model.modelName} validation failed: ${propertyName}: Path \`${propertyName}\` is required.`)
  })

  it('casts undefined to empty array', async () => {
    expect([...new Model({ [propertyName]: undefined })[propertyName]]).toEqual([])
    expect([...new Model()[propertyName]]).toEqual([])
  })

  if (emptyAllowed === false) {
    it('must not be empty array', async () => {
      expect(new Model({ [propertyName]: [] }).validateSync(propertyName)).toHaveProperty('message', `${Model.modelName} validation failed: ${propertyName}: Array must not be empty`)
    })
  } else {
    it('may be empty array', async () => {
      expect(new Model({ [propertyName]: [] }).validateSync(propertyName)).toBeUndefined()
    })
  }

  it('must not contain null', async () => {
    expect(new Model({ [propertyName]: [null] }).validateSync(propertyName)?.message).toBe(`${Model.modelName} validation failed: ${propertyName}: Value must be defined`)
  })

  it('must not contain undefined', async () => {
    expect(new Model({ [propertyName]: [undefined] }).validateSync(propertyName)?.message).toBe(`${Model.modelName} validation failed: ${propertyName}: Value must be defined`)
  })
}

export function nonBlankStringArrayTests (Model, propertyName) {
  it('casts single string value to singleton array', async () => {
    const model = new Model({ [propertyName]: '/single' })
    expect(model[propertyName]).toBeInstanceOf(Array)
    expect([...model[propertyName]]).toEqual(['/single'])
  })

  it('must not contain empty strings', async () => {
    expect(new Model({ [propertyName]: [''] }).validateSync(propertyName)).toHaveProperty('message', `${Model.modelName} validation failed: ${propertyName}: Value must not be a blank string`)
  })

  it('must not contain blank strings', async () => {
    expect(new Model({ [propertyName]: [' \t \n '] }).validateSync(propertyName)).toHaveProperty('message', `${Model.modelName} validation failed: ${propertyName}: Value must not be a blank string`)
  })

  it('allows non empty string array', async () => {
    expect(new Model({ [propertyName]: ['/foo'] }).validateSync(propertyName)).toBeUndefined()
  })
}

function requiredErrorMessage (Model, propertyName) {
  return `${Model.modelName} validation failed: ${propertyName}: Path \`${propertyName}\` is required.`
}

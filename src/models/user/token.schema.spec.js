import { describe, expect, it, jest } from '@jest/globals'
import mongoose from 'mongoose'
import { arrayPropertyTests, defaultPropertyTests, nonBlankStringArrayTests } from '../test.utils'
import { tokenSchema } from './token.schema'

describe('TokenSchema', () => {
  const Token = mongoose.model('Token', tokenSchema)

  describe('fields', () => {
    describe('_id', () => {
      defaultPropertyTests(Token, '_id', expect.any(mongoose.Types.ObjectId))
    })

    describe('scopes', () => {
      arrayPropertyTests(Token, 'scopes', { emptyAllowed: false })
      nonBlankStringArrayTests(Token, 'scopes')
    })
  })

  describe('virtuals', () => {
    describe('expired', () => {
      it(`returns false if not older than ${Token.EXPIRY_TIMEOUT / 1000} seconds`, () => {
        expect(new Token().expired).toBe(false)
      })

      it(`returns true if older than ${Token.EXPIRY_TIMEOUT / 1000} seconds`, () => {
        const token = new Token()
        jest.spyOn(Date, 'now').mockReturnValueOnce(Date.now() + Token.EXPIRY_TIMEOUT)
        expect(token.expired).toBe(true)
      })

      it('can not be set', () => {
        const token = new Token()
        const originalValue = token.expired
        token.expired = !originalValue
        expect(token.expired).toBe(originalValue)
      })
    })

    describe('expiresAt', () => {
      it('can not be set', () => {
        const token = new Token()
        const originalDate = token.expiresAt
        token.expiresAt = new Date()
        expect(token.expiresAt).toEqual(originalDate)
      })

      it(`calculates the expiresAt date as an offset of ${Token.EXPIRY_TIMEOUT} seconds after creation`, () => {
        const token = new Token()
        expect(token.expiresAt.getTime() - Date.now() - Token.EXPIRY_TIMEOUT).toBeLessThan(250) // Arbitrary value of quarter second difference is close enough
      })
    })

    describe('expiresIn', () => {
      it('can not be set', () => {
        const token = new Token()
        token.expiresIn = 100
        expect(token.expiresIn).toBe(3600)
      })

      it(`calculates expiresIn as remaining seconds until code expires`, () => {
        const token = new Token()
        expect(token.expiresIn).toBe(3600)
      })
    })
  })
})

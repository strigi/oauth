import { beforeEach, describe, expect, it, jest } from '@jest/globals'
import mongoose from 'mongoose'
import { codeSchema } from './code.schema'
import {
  arrayPropertyTests,
  defaultPropertyTests,
  mutablePropertyTests,
  nonBlankStringPropertyTests,
  requiredPropertyTests
} from '../test.utils'
import { Client } from '../client/client.model'

describe('CodeSchema', () => {
  const Code = mongoose.model('code', codeSchema)

  const client = new Client({
    name: 'MyClient',
    redirectUris: ['/foo'],
    scopes: ['profile']
  })

  describe('fields', () => {
    describe('_id', () => {
      defaultPropertyTests(Code, '_id', expect.any(mongoose.Types.ObjectId))
    })

    describe('redirectUri', () => {
      mutablePropertyTests(Code, 'redirectUri', { value: 'https://eothel.be/callback' })
      requiredPropertyTests(Code, 'redirectUri')
      nonBlankStringPropertyTests(Code, 'redirectUri')

      it('rejects values that do not look like an HTTP URI', () => {
        expect(new Code({ redirectUri: 'foo/bar' }).validateSync('redirectUri')?.message).toBe('code validation failed: redirectUri: Must start with \'http://\' \'https://\' or \'/\'')
        expect(new Code({ redirectUri: 'ssh://foo/bar' }).validateSync('redirectUri')?.message).toBe('code validation failed: redirectUri: Must start with \'http://\' \'https://\' or \'/\'')
        expect(new Code({ redirectUri: 'https:/foo/bar' }).validateSync('redirectUri')?.message).toBe('code validation failed: redirectUri: Must start with \'http://\' \'https://\' or \'/\'')

        expect(new Code({ redirectUri: '/foo/bar' }).validateSync('redirectUri')).toBeUndefined()
        expect(new Code({ redirectUri: 'http://foo/bar' }).validateSync('redirectUri')).toBeUndefined()
        expect(new Code({ redirectUri: 'https://foo/bar' }).validateSync('redirectUri')).toBeUndefined()
      })

      it('rejects URIs that have a fragment', () => {
        expect(new Code({ redirectUri: 'https://eothel.be#foo' }).validateSync('redirectUri')?.message).toBe('code validation failed: redirectUri: Must not have a URI fragment')
      })
    })

    describe('client', () => {
      mutablePropertyTests(Code, 'client', { value: client })
      requiredPropertyTests(Code, 'client')

      it('accepts ObjectId', () => {
        expect(new Code({ client: new mongoose.Types.ObjectId() }).validateSync('client')?.message).toBeUndefined()
      })
    })

    describe('tokens', () => {
      arrayPropertyTests(Code, 'tokens', { emptyAllowed: true })

      it('accepts valid Token object', () => {
        const code = new Code()
        code.tokens.push({ scopes: ['profile'] })
        expect(code.validateSync('tokens')).toBeUndefined()
      })
    })
  })

  describe('methods', () => {
    let code

    beforeEach(() => {
      code = new Code({
        client,
        redirectUri: client.redirectUris[0]
      })
    })

    describe('generateToken()', () => {
      it('returns the generated token', () => {
        const token = code.generateToken(['profile', 'email'])
        expect([...token.scopes]).toEqual(['profile', 'email'])
      })

      it('adds the generated token', () => {
        code.generateToken(['profile', 'email'])
        expect([...code.tokens[0].scopes]).toEqual(['profile', 'email'])
      })

      it('throws if no scopes were passed', () => {
        expect(() => {
          code.generateToken()
        }).toThrow('Argument \'scopes\' must be a non-empty array')
      })
    })
  })

  describe('virtuals', () => {
    describe('expired', () => {
      it(`returns false if not older than ${Code.EXPIRY_TIMEOUT / 1000} seconds`, () => {
        expect(new Code().expired).toBe(false)
      })

      it(`returns true if older than ${Code.EXPIRY_TIMEOUT / 1000} seconds`, () => {
        const code = new Code()
        jest.spyOn(Date, 'now').mockReturnValueOnce(Date.now() + Code.EXPIRY_TIMEOUT)
        expect(code.expired).toBe(true)
      })

      it('can not be set', () => {
        const code = new Code()
        const originalValue = code.expired
        code.expired = !originalValue
        expect(code.expired).toBe(originalValue)
      })
    })

    describe('consumed', () => {
      it('returns false if there are no tokens', () => {
        const code = new Code()
        expect(code.consumed).toBe(false)
      })

      it('returns true if there is at least one token', () => {
        const code = new Code()
        code.tokens.push({})
        expect(code.consumed).toBe(true)
      })

      it('can not be set', () => {
        const code = new Code()
        const originalBoolean = code.consumed
        code.consumed = !originalBoolean
        expect(code.consumed).toBe(originalBoolean)
      })
    })

    describe('expiresAt', () => {
      it('can not be set', () => {
        const code = new Code()
        const originalDate = code.expiresAt
        code.expiresAt = new Date()
        expect(code.expiresAt).toEqual(originalDate)
      })

      it(`calculates the expiresAt date as an offset of ${Code.EXPIRY_TIMEOUT} seconds after creation`, () => {
        const code = new Code()
        expect(code.expiresAt.getTime() - Date.now() - Code.EXPIRY_TIMEOUT).toBeLessThan(250) // Arbitrary value of quarter second difference is close enough
      })
    })

    describe('expiresIn', () => {
      it('can not be set', () => {
        const code = new Code()
        code.expiresIn = 100
        expect(code.expiresIn).toBe(600)
      })

      it(`calculates expiresIn as remaining seconds until code expires`, () => {
        const code = new Code()
        expect(code.expiresIn).toBe(600)
      })
    })
  })
})

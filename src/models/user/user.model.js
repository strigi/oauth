import mongoose from 'mongoose'
import crypto from 'crypto'
import { codeSchema } from './code.schema'
import { arrayValidator, definedValidator, validationSchemaObject } from '../validator'
import { logger } from '../../logger'
import { Client } from '../client/client.model'

const userSchema = new mongoose.Schema({
  firstName: {
    type: mongoose.SchemaTypes.String,
    required: true,
    trim: true
  },

  lastName: {
    type: mongoose.SchemaTypes.String,
    required: true,
    trim: true
  },

  email: {
    type: mongoose.SchemaTypes.String,
    required: true,
    trim: true
  },

  /**
   * TODO: enforce password strength
   * TODO: implement salt
   */
  password: {
    type: mongoose.SchemaTypes.String,
    required: true,
    set (password) {
      return hash(password)
    }
  },

  codes: {
    type: [codeSchema],
    required: true,
    validate: validationSchemaObject(arrayValidator([definedValidator()], true))
  }
})

userSchema.virtual('username').get(function() {
  return this.email;
});

userSchema.methods.passwordEquals = function (plainTextPassword) {
  if (typeof plainTextPassword !== 'string') {
    throw new Error('Argument \'password\' must be a string')
  }
  return this.password === hash(plainTextPassword)
}

/**
 * Generates a new authorization code and saves it in the user.
 * @param client {Client} The client to generate a token for.
 * @param redirectUri {string} The redirectUri to bind with this token (must also exist in `client.redirectUris`
 * @return The new code as a Mongoose Embedded document.
 */
userSchema.methods.generateCode = function(client, redirectUri) {
  if(!client || !(client instanceof Client)) {
    throw new Error(`Argument 'client' must be an instance of 'Client' mongoose model`)
  }

  if(typeof redirectUri !== 'string') {
    throw new Error(`Argument 'redirectUri' must be a string`)
  }

  if(!client.redirectUris.includes(redirectUri)) {
    throw new Error(`redirectUri '${redirectUri}' is not valid for client '${client.name}' any of [${client.redirectUris.join(', ')}] expected`);
  }

  const code = this.codes.create({ client, redirectUri });
  this.codes.push(code);
  return code;
}

userSchema.methods.codeById = function (codeId) {
  if(!(codeId instanceof mongoose.Types.ObjectId) && !mongoose.Types.ObjectId.isValid(codeId)) {
    return null;
  }

  const codeObjectId = new mongoose.Types.ObjectId(codeId);
  return this.codes.find(c => c._id.toString() === codeObjectId.toString()) || null;
}

userSchema.statics.findByUsernameAndPassword = async function(username, password) {
  const user = await this.findOne({ email: username })
  if(!user) {
    logger.debug(`User '${username}' does not exist`)
    return null;
  }

  if(!user.passwordEquals(password)) {
    logger.debug(`User '${username}' exists but password was invalid`)
    return null;
  }
  return user;
}

userSchema.statics.findByCode = async function (codeId) {
  if(!mongoose.Types.ObjectId.isValid(codeId)) {
    logger.debug(`Authorization code '${codeId}' is not a valid ObjectId.`);
    // TODO: throw?
    return null;
  }

  const user = await this.findOne({'codes._id': codeId})
  if(!user) {
    logger.debug(`No user found that has an authorization code '${codeId}'`);
    return null;
  }

  const code = user.codeById(codeId);
  if(code.expired === true) {
    logger.debug(`Authorization code '${codeId}' belongs to user '${user.username}' but has been expired.`);
    return null;
  }

  if(code.consumed) {
    // TODO: revoke all tokens!!!!
    logger.debug(`Authorization code '${codeId}' was already consumed.`);
    return null;
  }

  logger.debug(`User '${user.username}' is found to have non-expired authorization code '${codeId}'`);
  return user
}

export const User = mongoose.model('User', userSchema)

function hash (value) {
  if (!value) {
    return value
  }
  return crypto.createHash('sha256').update(value).digest('hex')
}

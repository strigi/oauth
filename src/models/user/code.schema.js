import mongoose from 'mongoose'
import { Client } from '../client/client.model'
import { tokenSchema } from './token.schema'
import { arrayValidator, definedValidator, uriValidator, validationSchemaObject } from '../validator'

export const codeSchema = new mongoose.Schema({
  redirectUri: {
    type: mongoose.SchemaTypes.String,
    required: true,
    trim: true,
    validate: validationSchemaObject(uriValidator())
  },

  client: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: Client.modelName,
    required: true
  },

  tokens: {
    type: [tokenSchema],
    required: true,
    validate: validationSchemaObject(arrayValidator([definedValidator()], { emptyAllowed: true }))
  },
})

/**
 * The Oauth 2.1 spec recommends a maximum expiry timeout for access tokens of 10 minutes.
 * @see https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-4.1.2-2.2
 * @type {number}
 */
codeSchema.statics.EXPIRY_TIMEOUT = 10 * 60 * 1000

codeSchema.virtual('expired').get(function () {
  return this.expiresAt.getTime() < Date.now()
})

codeSchema.virtual('expiresAt').get(function() {
  return new Date(this._id.getTimestamp().getTime() + codeSchema.statics.EXPIRY_TIMEOUT);
});

codeSchema.virtual('expiresIn').get(function() {
  return Math.ceil((this.expiresAt.getTime() - Date.now()) / 1000);
});

codeSchema.virtual('consumed').get(function () {
  return this.tokens.length > 0
})

codeSchema.methods.generateToken = function(scopes) {
  if(!scopes || !Array.isArray(scopes) || scopes.length === 0) {
    throw new Error(`Argument 'scopes' must be a non-empty array`)
  }
  const token = this.tokens.create({
    scopes
  })
  this.tokens.push(token)
  return token;
}
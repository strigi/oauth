import mongoose from 'mongoose'
import { arrayValidator, notBlankStringValidator, validationSchemaObject } from '../validator'

export const tokenSchema = new mongoose.Schema({
  scopes: {
    type: [mongoose.SchemaTypes.String],
    required: true,
    validate: validationSchemaObject(arrayValidator([notBlankStringValidator()]))
  }
})

tokenSchema.statics.EXPIRY_TIMEOUT = 3600 * 1000

// TODO: duplicates with Code refactor extract
tokenSchema.virtual('expiresAt').get(function() {
  return new Date(this._id.getTimestamp().getTime() + tokenSchema.statics.EXPIRY_TIMEOUT);
});

tokenSchema.virtual('expiresIn').get(function() {
  return Math.ceil((this.expiresAt.getTime() - Date.now()) / 1000);
});

tokenSchema.virtual('expired').get(function () {
  return this.expiresAt.getTime() < Date.now()
})
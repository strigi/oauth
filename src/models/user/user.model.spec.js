import { beforeEach, describe, expect, it, jest } from '@jest/globals'
import { User } from './user.model'
import {
  arrayPropertyTests,
  defaultPropertyTests,
  mutablePropertyTests,
  nonBlankStringPropertyTests,
  requiredPropertyTests
} from '../test.utils'
import { Client } from '../client/client.model'
import mongoose from 'mongoose'
import { setupMemoryMongo } from '../../memory-mongo'
import { newValidClient } from '../client/client.fixtures'
import { newValidUser } from './user.fixtures'

describe('User', () => {
  describe('fields', () => {
    describe('_id', () => {
      defaultPropertyTests(Client, '_id', expect.any(mongoose.Types.ObjectId))
    })

    describe('firstName', () => {
      mutablePropertyTests(User, 'firstName')
      requiredPropertyTests(User, 'firstName')
      nonBlankStringPropertyTests(User, 'firstName')
    })

    describe('lastName', () => {
      mutablePropertyTests(User, 'lastName')
      requiredPropertyTests(User, 'lastName')
      nonBlankStringPropertyTests(User, 'lastName')
    })

    describe('email', () => {
      mutablePropertyTests(User, 'email')
      requiredPropertyTests(User, 'email')
      nonBlankStringPropertyTests(User, 'email')
    })

    describe('password', () => {
      requiredPropertyTests(User, 'password')

      it('hashes the password when created', () => {
        const user = new User({ password: 'hello' })
        expect(user.password).toBe('2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824')
      })

      it('hashes the password when updated', () => {
        const user = new User()
        user.password = 'world'
        expect(user.password).toBe('486ea46224d1bb4fb680f34f7c9ad96a8f24ec88be73ea8e5a6c65260e9cb8a7')
      })
    })

    describe('passwordEquals()', () => {
      it('returns false if password does not match', () => {
        expect(new User({ password: 'foo' }).passwordEquals('bar')).toBe(false)
      })

      it('returns true if password matches', () => {
        expect(new User({ password: 'foo' }).passwordEquals('foo')).toBe(true)
      })

      it('throws if argument \'password\' is not a string', () => {
        expect(() => {
          new User().passwordEquals(1507)
        }).toThrow('Argument \'password\' must be a string')
      })
    })

    describe('codes', () => {
      arrayPropertyTests(User, 'codes', { emptyAllowed: true })

      it('rejects invalid subdocuments', () => {
        const user = new User()
        const invalidCode = user.codes.create({})
        user.codes.push(invalidCode)
        expect(user.validateSync('codes')?.message).toBe('User validation failed: codes.0.client: Path `client` is required., codes.0.redirectUri: Path `redirectUri` is required.')
      })

      it('accepts valid subdocuments', () => {
        const user = new User()
        const validCode = user.codes.create({
          client: new mongoose.Types.ObjectId(),
          scopes: ['profile'],
          redirectUri: '/foo'
        })
        user.codes.push(validCode)
        expect(user.validateSync('codes')?.message).toBeUndefined()
      })
    })
  })

  describe('database', () => {
    setupMemoryMongo()

    let client

    beforeEach(async () => {
      client = await new Client({
        name: 'Eothel',
        redirectUris: ['https://eothel.be/callback'],
        scopes: ['profile']
      }).save()
    })

    it('User can be given a code and tokens and then saved', async () => {
      const user = new User({
        firstName: 'Jimi',
        lastName: 'Hendrix',
        email: 'jimi.hendrix@eothel.be',
        password: 'W00d:st0ck!'
      })

      const code = user.codes.create({
        client,
        redirectUri: 'https://eothel.be/callback',
      })
      code.tokens.push({ scopes: ['profile'] })
      code.tokens.push({ scopes: ['profile', 'email'] })

      user.codes.push(code)

      await user.save()

      const persistedUser = await User.findById(user._id)
      expect(persistedUser.codes?.length).toBe(1)
      expect(persistedUser.codes[0].tokens?.length).toBe(2)
    })

    it('User can be initialized with a code and tokens and then saved', async () => {
      const user = await new User({
        firstName: 'Jimi',
        lastName: 'Hendrix',
        email: 'jimi.hendrix@eothel.be',
        password: 'W00d:st0ck!',
        codes: [{
          client,
          redirectUri: 'https://eothel.be/callback',
          tokens: [{
            scopes: ['profile'],
          }]
        }, {
          client,
          redirectUri: 'https://eothel.be/callback',
          tokens: [{
            scopes: ['profile'],
          }, {
            scopes: ['profile'],
          }]
        }]
      }).save()

      const persistedUser = await User.findById(user._id)
      expect(persistedUser.codes?.length).toBe(2)
      expect(persistedUser.codes[0].tokens?.length).toBe(1)
      expect(persistedUser.codes[1].tokens?.length).toBe(2)
    })
  })

  describe('virtuals', () => {
    it('aliases email as username', () => {
      expect(newValidUser({ email: 'foo@bar.be' }).model.username).toBe('foo@bar.be')
    })
  })

  describe('methods', () => {
    const client = newValidClient({ name: 'Foo', redirectUris: ['https://foo.be/callback'] }).model

    describe('passwordEquals()', () => {
      it('returns true if the user password matches', () => {
        const user = newValidUser({ password: 'foo' }).model
        expect(user.password).not.toBe('foo')
        expect(user.passwordEquals('foo')).toBe(true)
      })

      it('returns false if the user password matches', () => {
        const user = newValidUser({ password: 'foo' }).model
        expect(user.password).not.toBe('foo')
        expect(user.passwordEquals('bar')).toBe(false)
      })
    })

    describe('generateCode()', () => {
      let user

      beforeEach(() => {
        user = newValidUser().model
      })

      it('adds a code to the user', () => {
        const code = user.generateCode(client, 'https://foo.be/callback')
        expect(code._id).toBeDefined()
        expect(code.client.name).toBe('Foo')
        expect(code.client.redirectUris[0]).toBe('https://foo.be/callback')
      })

      it('returns the created code', () => {
        const code = user.generateCode(client, 'https://foo.be/callback')
        expect(user.codes.length).toBe(1)
      })

      it('throws if the redirectUri does not match the client', () => {
        expect(() => {
          user.generateCode(client, 'https://bar.be/callback')
        }).toThrow('Foo')
      })

      it('throws if redirectUri is missing', () => {
        expect(() => {
          user.generateCode(client)
        }).toThrow('Argument \'redirectUri\' must be a string')
      })

      it('throws if redirectUri is not a string', () => {
        expect(() => {
          user.generateCode(client, new Date())
        }).toThrow('Argument \'redirectUri\' must be a string')
      })

      it('throws if client is missing is not an instance of Client', () => {
        expect(() => {
          user.generateCode('this is not a Client', 'https://foo.be/callback')
        }).toThrow('Argument \'client\' must be an instance of \'Client\' mongoose model')
      })
    })

    describe('codeById()', () => {
      let user;
      let codeId;

      beforeEach(() => {
        user = newValidUser().model
        const code = user.codes.create({
          client,
          redirectUri: client.redirectUris[0]
        });
        user.codes.push(code);
        codeId = code._id;
      })

      it('returns code if passed a valid ObjectId or string for a code that exists', () => {
        expect(user.codeById(codeId).redirectUri).toBe('https://foo.be/callback');
        expect(user.codeById(codeId.toString()).redirectUri).toBe('https://foo.be/callback');
      })

      it('returns null if passed an ObjectId or string for a code does not exist', () => {
        expect(user.codeById('non existing id')).toBeNull();
        expect(user.codeById(new mongoose.Types.ObjectId())).toBeNull();
      })
    })
  })

  describe('statics', () => {
    setupMemoryMongo()

    describe('findByUsernameAndPassword()', () => {
      beforeEach(async () => {
        await newValidUser().model.save()
      })

      it('retrieve user with valid username and password', async () => {
        const user = await User.findByUsernameAndPassword('jimi.hendrix@eothel.be', 'W00d:st0ck!')
        expect(user.firstName).toBe('Jimi')
        expect(user.lastName).toBe('Hendrix')
        expect(user.email).toBe('jimi.hendrix@eothel.be')
      })

      it('does not retrieve user with invalid password', async () => {
        const user = await User.findByUsernameAndPassword('jimi.hendrix@eothel.be', 'wrong password')
        expect(user).toBeNull()
      })

      it('does not retrieve user non-existing user', async () => {
        const user = await User.findByUsernameAndPassword('janis.joplin@eothel.be', 'does not matter')
        expect(user).toBeNull()
      })
    })

    describe('findByCode()', () => {
      let code

      beforeEach(async () => {
        const user = await newValidUser().model
        code = user.generateCode(await newValidClient().model.save(), 'https://eothel.be/callback')._id
        await user.save()
      })

      it('returns null if user with code was not found', async () => {
        const user = await User.findByCode(new mongoose.Types.ObjectId())
        expect(user).toBeNull()
      })

      it('returns null if code is not a valid ObjectId', async () => {
        const user = await User.findByCode('this is not a valid ObjectId')
        expect(user).toBeNull()
      })

      it('returns User if user with code was found', async () => {
        const user = await User.findByCode(code)
        expect(user.firstName).toBe('Jimi')
        expect(user.codes[0].redirectUri).toBe('https://eothel.be/callback')
      })

      it('returns null if the code is expired', async () => {
        jest.spyOn(Date, 'now').mockReturnValue(Date.UTC(2099, 6, 15, 17, 15))
        const user = await User.findByCode(code)
        expect(user).toBeNull()
      })

      it('returns null if the code is already consumed', async () => {
        const client = await newValidClient({ name: 'Foo', redirectUris: ['https://foo.be/callback'] }).model.save()
        const user = newValidUser().model;
        const code = user.generateCode(client, client.redirectUris[0]);
        code.generateToken(['profile'])
        await user.save();
        expect(await User.findByCode(code._id)).toBeNull()
      })
    })
  })
})

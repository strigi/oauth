import { User } from './user.model'
import { fixture } from '../model.utils'

const defaultDoc = {
  firstName: 'Jimi',
  lastName: 'Hendrix',
  email: 'jimi.hendrix@eothel.be',
  password: 'W00d:st0ck!',
}

export function newValidUser (doc) {
  return fixture(User, { ...defaultDoc, ...doc })
}

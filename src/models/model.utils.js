export function preventWriteSetter (_value, property) {
  throw new Error(`Property '${property.path}' can not be written`)
}

export function fixture (Model, doc) {
  return {
    get doc () {
      return doc
    },

    get model () {
      return new Model(doc)
    }
  }
}
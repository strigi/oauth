import { Client } from './client.model'
import { fixture } from '../model.utils'

const defaultDoc = {
  name: 'Eothel',
  redirectUris: [
    'https://eothel.be/callback',
    'https://qa.eothel.be/callback',
    'http://localhost:3000/callback'
  ],
  scopes: [
    'profile',
    'email',
  ],
}

export function newValidClient (doc) {
  return fixture(Client, { ...defaultDoc, ...doc })
}


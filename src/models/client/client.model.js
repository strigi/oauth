import mongoose from 'mongoose'
import crypto from 'crypto'
import { arrayValidator, notBlankStringValidator, uriValidator, validationSchemaObject } from '../validator'
import { logger } from '../../logger'

export const clientSchema = new mongoose.Schema({
  name: {
    type: mongoose.SchemaTypes.String,
    required: true,
    trim: true
  },

  secret: {
    type: mongoose.SchemaTypes.String,
    required: true,
    default: crypto.randomBytes(24).toString('hex')
  },

  redirectUris: {
    type: [mongoose.SchemaTypes.String],
    required: true,
    validate: validationSchemaObject(arrayValidator([notBlankStringValidator(), uriValidator()]))
  },

  scopes: {
    type: [mongoose.SchemaTypes.String],
    required: true,
    validate: validationSchemaObject(arrayValidator([notBlankStringValidator()]))
  }
})

clientSchema.statics.findByIdAndRedirectUri = async function (clientId, redirectUri) {
  const client = await Client.findById(clientId)
  if(!client) {
    logger.debug(`Client '${clientId}' does not exist.`)
    return null;
  }

  if(!redirectUri) {
    if(client.redirectUris.length === 1) {
      logger.debug(`Missing redirect_uri but client '${clientId}' has only one registered '${client.redirectUris[0]}' is allowed by the spec.`)
      return client;
    } else {
      logger.debug(`Client ${clientId} redirect_uri is ambiguous.`)
      return null;
    }
  }

  if(!client.redirectUris.includes(redirectUri)) {
    logger.debug(`Client '${clientId}' does not have redirect_uri '${redirectUri}'`)
    return null;
  }

  return client
}

clientSchema.statics.findByIdAndSecret = function(clientId, clientSecret) {
  return this.findOne({_id: clientId, secret: clientSecret});
}

export const Client = mongoose.model('Client', clientSchema)

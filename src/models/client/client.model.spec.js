import { describe, expect, it, jest } from '@jest/globals'
import mongoose from 'mongoose'
import { Client } from './client.model'
import {
  arrayPropertyTests,
  defaultPropertyTests,
  mutablePropertyTests,
  nonBlankStringArrayTests,
  nonBlankStringPropertyTests,
  requiredPropertyTests
} from '../test.utils'
import { setupMemoryMongo } from '../../memory-mongo'
import { newValidClient } from './client.fixtures'

describe('Client', () => {
  describe('fields', () => {
    describe('_id', () => {
      defaultPropertyTests(Client, '_id', expect.any(mongoose.Types.ObjectId))
    })

    describe('name', () => {
      mutablePropertyTests(Client, 'name')
      requiredPropertyTests(Client, 'name')
      nonBlankStringPropertyTests(Client, 'name')
    })

    describe('secret', () => {
      mutablePropertyTests(Client, 'secret')
      requiredPropertyTests(Client, 'secret')
      defaultPropertyTests(Client, 'secret')

      it('defaults to hex string', () => {
        expect(new Client({}).secret).toMatch(/^[0-9a-f]{48}$/)
        expect(new Client({ secret: undefined }).secret).toMatch(/^[0-9a-f]{48}$/)
      })
    })

    describe('redirectUris', () => {
      arrayPropertyTests(Client, 'redirectUris')
      nonBlankStringArrayTests(Client, 'redirectUris')

      it('rejects values that do not look like an HTTP URI', () => {
        expect(new Client({ redirectUris: ['foo/bar'] }).validateSync('redirectUris')?.message).toBe('Client validation failed: redirectUris: Must start with \'http://\' \'https://\' or \'/\'')

        expect(new Client({
          redirectUris: [
            '/foo',
            'bar'
          ]
        }).validateSync('redirectUris')?.message).toBe('Client validation failed: redirectUris: Must start with \'http://\' \'https://\' or \'/\'')

        expect(new Client({
          redirectUris: [
            'http://foo',
            'https:/foo/bar'
          ]
        }).validateSync('redirectUris')?.message).toBe('Client validation failed: redirectUris: Must start with \'http://\' \'https://\' or \'/\'')

        expect(new Client({ redirectUris: ['/foo/bar'] }).validateSync('redirectUris')).toBeUndefined()
        expect(new Client({ redirectUris: ['https://foo/bar'] }).validateSync('redirectUris')).toBeUndefined()
        expect(new Client({ redirectUris: ['http://foo/bar'] }).validateSync('redirectUris')).toBeUndefined()
      })

      it('rejects values that have a fragment URI', () => {
        const error = new Client({
          redirectUris: ['https://foo/bar#fragment']
        }).validateSync('redirectUris')
        expect(error?.message).toBe('Client validation failed: redirectUris: Must not have a URI fragment');
      })
    })

    describe('scopes', () => {
      arrayPropertyTests(Client, 'scopes')
      nonBlankStringArrayTests(Client, 'scopes')
    })
  })

  describe('database', () => {
    setupMemoryMongo()

    it('can be saved', async () => {
      const client = await new Client({
        name: 'Eothel',
        redirectUris: ['https://eothel.be/callback'],
        scopes: ['profile']
      }).save()

      const persistedClient = await Client.findById(client._id)
      expect(persistedClient.name).toBe('Eothel')
      expect([...persistedClient.redirectUris]).toEqual(['https://eothel.be/callback'])
      expect([...persistedClient.scopes]).toEqual(['profile'])
    })
  })

  describe('statics', () => {
    describe('findByIdAndRedirectUri()', () => {
      it('returns null if id does not exist', async () => {
        jest.spyOn(Client, 'findById').mockResolvedValue(null)
        const client = await Client.findByIdAndRedirectUri('myId', 'https://eothel.be/callback')
        expect(client).toBe(null)
      })

      it('returns null if client exists but does not have redirectUri', async () => {
        const client = newValidClient({
            redirectUris: ['https://eothel.be/callback']
        }).model;
        jest.spyOn(Client, 'findById').mockResolvedValue(client)
        const foundClient = await Client.findByIdAndRedirectUri(client._id, 'https://qa.eothel.be/callback')
        expect(foundClient).toBe(null)
      })

      it('returns client if client with redirectUri exists', async () => {
        const client = newValidClient({
            redirectUris: ['https://eothel.be/callback']
        }).model;
        jest.spyOn(Client, 'findById').mockResolvedValue(client)
        const foundClient = await Client.findByIdAndRedirectUri(client._id, 'https://eothel.be/callback')
        expect(foundClient.toObject()).toEqual(client.toObject())
      })
    })
  })
})

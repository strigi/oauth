export function arrayValidator (itemValidators = [], emptyAllowed = false) {
  return values => {
    if (!emptyAllowed && values.length === 0) {
      throw new Error('Array must not be empty')
    }

    for (const value of values) {
      for (const validator of itemValidators) {
        validator(value)
      }
    }
  }
}

export function definedValidator () {
  return value => {
    if (value === null || value === undefined) {
      throw new Error('Value must be defined')
    }
  }
}

export function notBlankStringValidator () {
  return value => {
    definedValidator()(value)
    typeValidator('string')(value)
    if (value.trim() === '') {
      throw new Error('Value must not be a blank string')
    }
  }
}

export function typeValidator (type) {
  return value => {
    // eslint-disable-next-line valid-typeof
    if (typeof value !== type) {
      throw new Error(`Value '${value}' must be of type '${type}'`)
    }
  }
}

export function instanceValidator (constructor) {
  return value => {
    if (!(value instanceof constructor)) {
      throw new Error(`Value '${value}' must be instance of '${constructor?.name}'`)
    }
  }
}

export function regexValidator (pattern, message, {inverse = false } = {}) {
  return value => {
    definedValidator()(value)
    const match = value.match(pattern)
    if ((!inverse && !match) || (inverse && match)) {
      throw new Error(message)
    }
  }
}

export function uriValidator () {
  return value => {
    regexValidator(/^(https?:\/)?\//, 'Must start with \'http://\' \'https://\' or \'/\'')(value)
    regexValidator(/#\w*$/, 'Must not have a URI fragment', { inverse: true })(value)
  }
}

export function validationSchemaObject (validator) {
  return {
    validator,
    message (props) {
      return props.reason.message
    }
  }
}

import express from 'express'
import authorize from './oauth-authorize'
import * as token from './token/token.router'
import * as auth from './authorize/authorize.router'
import { methodNotAllowed } from './method-not-allowed-handler'

export const AUTHORIZE_PATH = '/authorize'
export const USERINFO_PATH = '/userinfo'
export const TOKEN_PATH = '/token'

export const router = express.Router()

router.use(AUTHORIZE_PATH, auth.router);

router.use(TOKEN_PATH, token.router)

router.route(USERINFO_PATH).get(authorize(), async (req, res) => {
  res.json({
    username: req.user.username,
    email: req.user.email,
    firstName: req.user.firstName,
    lastName: req.user.lastName,
    locale: req.user.locale
  })
}).all(methodNotAllowed)

import express from 'express'
import { HttpResponseError } from '../http/error'
import { file } from '../file'
import { OAuthClient } from '../oauth-client'
import { logger } from '../logger'
import { User } from '../models/user/user.model'
import { methodNotAllowed } from '../method-not-allowed-handler'
import { Client, clientSchema } from '../models/client/client.model'
import { requestParameter } from '../http/parameters'

export const router = express.Router()

// router.route('/').get((req, res) => {
//   const clientId = req.query.client_id
//   const redirectUri = req.query.redirect_uri
//   if (!clientId || !redirectUri) {
//     throw HttpResponseError.fromScratch('Query string must contain both \'client_id\' and \'redirect_uri\' parameters', 400)
//   }
//   res.sendFile(file(import.meta.url).join('public/auth.html'))
// }).post(express.json(), express.urlencoded({ extended: true }), async (req, res, next) => {
//   // Accept only clients that are registered in our database.
//   const clientId = req.query.client_id || req.body.client_id
//   if (!clientId) {
//     throw HttpResponseError.fromScratch('Query string must contain \'client_id\' parameter', 400)
//   }
//   const client = await OAuthClient.findByClientId(clientId)
//   if (!client) {
//     throw HttpResponseError.fromScratch(`Unknown client_id '${clientId}'`, 400)
//   }
//
//   // Accept the client only if the requested redirectUri matches with the registered client in the database
//   const redirectUri = req.query.redirect_uri || req.body.redirect_uri
//   if (!redirectUri) {
//     throw HttpResponseError.fromScratch('redirect_uri is mandatory', 400)
//   }
//   if (!client.redirectUris.includes(redirectUri)) {
//     logger.debug(`Rejecting authentication due to invalid redirect_uri '${redirectUri}' for client '${clientId}'`)
//     throw HttpResponseError.fromScratch(`Invalid redirect_uri '${redirectUri}'`, 400)
//   }
//
//   // Perform user authentication by either username+password or existing access token.
//   const username = req.body.username
//   const password = req.body.password
//   const accessToken = req.body.access_token
//   if ((!username || !password) && !accessToken) {
//     throw HttpResponseError.fromScratch('Either (username and password) or accessToken is expected', 400)
//   }
//   let user
//   if (accessToken) {
//     logger.debug(`Attempting to authenticate user by existing access token '${accessToken}'`)
//     user = await User.findByValidAccessToken(accessToken)
//   } else {
//     logger.debug(`Attempting to authenticate user by username/password '${username}/***'`)
//     user = await User.findByUsernameAndPassword(username, password)
//   }
//
//   if (!user) {
//     throw HttpResponseError.fromScratch(`Authentication failed for user or access token '${username || accessToken}'`, 401)
//   }
//
//   // Generate an authorization token
//   const code = user.addNewAuthorizationCode(client, redirectUri)
//   await user.save()
//
//   // Send the browser a redirect URL to the client's requested redirectURL
//   // TODO: if it has query string, retain it and add authorization code with '&'
//   logger.info(`Authentication successful for user '${user.username}'. Offering authorization code '${code}'`)
//   // res.redirect(); // fetch does not allow manual looking at Location header when 302 https://github.com/whatwg/fetch/issues/763
//   res.status(200).location(redirectUri + `?code=${code}`).send()
// }).all(methodNotAllowed)

// TODO: Spec: MUST support GET (how to pass user credentials? Via addiotional redirect flow UI???)
// DONE: Spec: MAY support POST
// DONE: Spec: parameters MUST be included only once
// DONE: Spec: empty parameters MUST be ignored
// DONE: Spec: unrecognized parameters MUST be ignored
// DONE: Spec: must have response_type=code
// TODO: Spec: extensions of response_type parameter extensions may add extras values and lists that are space delimited
// DONE: Spec: redirect_uri MUST be compared with Client
// DONE: Spec: redirect_uri MUST not contain #fragment (implement on Client and Code)
// TODO: Spec: redirect_uri MAY contain query string and it MUST be preserved when extra's are added to respond
// TODO: Spec: redirect_uri SHOULD be https and if not a warning SHOULD be displayed to the user
// TODO: Spec: MAY add state parameter (?clarification needed?)
// TODO: Spec: MUST code_challenge PKCE (chapter 4)
// DONE: Spec: if only one redirect_uri is registered, it MAY be omitted by the client
// DONE: Spec: invalid redirect_uri MUST NOT be redirected to and SHOULD be warned to the client
// TODO: error handling according to spec?
// TODO: Spec: if response_type is not recognized MUST respond error 4.1.2.1
// TODO: Spec: Add Cache-Control and Pragma headers as per spec https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-5.1-4 (is this needed for the /auth endpoint or only the /token?)
// TODO: Spec: resue of the same code -> deny and revoke all access tokens
// DONE: Spec: recommended expiry of code 10 minutes
// TODO: Spec: state parameter -> echo in request to response (e.g. correlation Id)
router.route('/').post(express.urlencoded({ extended: true }), async (req, res) => {
  // Validate response_type parameter. Current implementation only supports "code" (the only one mandatory by the spec)
  const responseType = requestParameter(req.query, 'response_type')
  if (responseType !== 'code') {
    throw new Error(`response_type must be 'code'`)
  }

  // TODO: code challenge MUST have for 2.1
  // code_challenge
  // code_challenge_method

  // Retrieve client
  const clientId = requestParameter(req.query, 'client_id')
  const redirectUri = requestParameter(req.query, 'redirect_uri', { required: false })
  const client = await Client.findByIdAndRedirectUri(clientId, redirectUri);
  if(!client) {
    throw new Error(`client_id '${clientId}' does not exist or does not match with redirect_uri '${redirectUri}'`);
  }

  // Authenticate user
  const { username, password } = req.body
  if (!username || !password) {
    throw new Error('Query parameters username and password are required');
  }
  const user = await User.findByUsernameAndPassword(username, password)
  if (!user) {
    throw new Error(`User ${user.username} not found or invalid password`);
  }

  // Create an authorization code
  const boundRedirectUri = redirectUri || client.redirectUris[0]
  const code = user.codes.create({
    client,
    redirectUri: boundRedirectUri,
    scopes: [...client.scopes] // TODO: Does scopes really have to exist on the code, or on the token?
  })
  user.codes.push(code)
  await user.save()

  // Send redirect response
  const url = new URL(boundRedirectUri);
  url.searchParams.set('authorization_code', code.id)
  res.redirect(url.toString());

}).all(methodNotAllowed)


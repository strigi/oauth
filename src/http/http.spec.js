import { describe, expect, it } from '@jest/globals'
import { parseAuthorizationHeader } from './http'

describe('parseAuthorizationHeader()', () => {
  it('returns username and password', () => {
    const result = parseAuthorizationHeader(buildBasicAuthorizationHeaderValue('myUsername', 'myPassword'))
    expect(result).toHaveProperty('username', 'myUsername')
    expect(result).toHaveProperty('password', 'myPassword')
  })

  it('allows : in password', () => {
    const result = parseAuthorizationHeader(buildBasicAuthorizationHeaderValue('myUsername', 'my:Password'))
    expect(result).toHaveProperty('username', 'myUsername')
    expect(result).toHaveProperty('password', 'my:Password')
  })

  it('throws error when null', () => {
    expect(() => {
      parseAuthorizationHeader(null)
    }).toThrow('Authorization header must be of type \'string\' but was \'null\'')
  })

  it('throws error when undefined', () => {
    expect(() => {
      parseAuthorizationHeader(undefined)
    }).toThrow('Authorization header must be of type \'string\' but was \'undefined\'')
  })

  it('throws error when not a string', () => {
    expect(() => {
      parseAuthorizationHeader(1507)
    }).toThrow('Authorization header must be of type \'string\' but was \'number\'')
  })

  it('throws error when empty', () => {
    expect(() => {
      parseAuthorizationHeader('')
    }).toThrow('Authorization header must not be empty but was \'\'')
  })

  it('throws error when whitespace only', () => {
    expect(() => {
      parseAuthorizationHeader(' \t \n ')
    }).toThrow('Authorization header must not be empty but was \' \t \n \'')
  })

  it('throws error when header is invalid', () => {
    expect(() => {
      parseAuthorizationHeader('this is a totally bogus header value')
    }).toThrow('Authorization header \'this is a totally bogus header value\' is invalid')
  })

  it('throws error when scheme is not _Basic_', () => {
    expect(() => {
      parseAuthorizationHeader('Digest FOO')
    }).toThrow('Require \'Basic\' Authorization scheme but was \'Digest\'')
  })

  it('throws error when credentials do not contain a :', () => {
    expect(() => {
      parseAuthorizationHeader('Basic ' + Buffer.from('foobar').toString('base64'))
    }).toThrow('Authorization header credentials must contain a separator \':\'')
  })

  it('throws error when username is empty', () => {
    expect(() => {
      parseAuthorizationHeader(buildBasicAuthorizationHeaderValue('', 'myPassword'))
    }).toThrow('Authorization header username and password must not be empty')
  })

  it('throws error when password is empty', () => {
    expect(() => {
      parseAuthorizationHeader(buildBasicAuthorizationHeaderValue('myUsername', ''))
    }).toThrow('Authorization header username and password must not be empty')
  })
})

function buildBasicAuthorizationHeaderValue (username, password) {
  const credentials = Buffer.from(`${username}:${password}`).toString('base64')
  return `Basic ${credentials}`
}

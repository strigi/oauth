import { promises } from 'fs'
import { logger } from '../logger'
import { file } from '../file'

const htmlTemplateString = (await promises.readFile(file(import.meta.url).join('error-template.html'))).toString()

/**
 * Represents all HTTP type errors.
 * This class is intended to be used in controllers (but not deeper). To simplify HTTP status code based error handling.
 * It goes hand in hand with an _Express_ middleware {@link HttpResponseError.middleware} so that controllers can just
 * throw and exit the executing function. Particularly with Express 5 this will make error handling much more convenient.
 */
export class HttpResponseError extends Error {
  /**
   * Construct a new HttpResponseError from an underlying error (i.e. from the service or database layer).
   * The private message (printed in the logs) will be set to `error.message`.
   * These are supposed to be unexpected errors, and this the HTTP status code will be set to 500.
   * @param error Causing error.
   * @param publicMessage Message to be shown on the HTTP response. This should **NEVER** contain any technical
   * details for security reasons. Defaults to 'Internal Server Error'
   * @return a new Error instance ready to throw (and caught by {@link HttpResponseError.middleware}
   */
  static fromError (error, publicMessage = 'Internal Server Error') {
    return new HttpResponseError(publicMessage, 500, { cause: error })
  }

  /**
   * Construct a new HttpResponseError from nothing. Typically useful for validation of input parameters and other
   * pre-conditions. Therefore it allows setting custom status code (i.e. 400, 401, ...)
   * @param publicMessage The message that will be shown on the HTTP response (don't use for technical details due to
   * security!)
   * @param statusCode The HTTP status code to use.
   * @param options
   * @return A new Error instance ready to throw (and caught by {@link HttpResponseError.middleware}
   */
  static fromScratch (publicMessage, statusCode, { privateMessage } = {}) {
    return new HttpResponseError(publicMessage, statusCode, { privateMessage })
  }

  /**
   * Express middleware that can be used as an error handler.
   * It will log output of 500 as error and others as info. The logs include the full detail information like stack etc...
   * Only when in non-production mode, it will write the stacktrace to the response (for easier debugging).
   * If the response accepts text/html it will render a HTML template, otherwise application/json is used.
   * @param error both HttpRequestError and regular Error are allowed.
   * @param req _Express_ request
   * @param res _Express_ response
   * @param _next Express's function to call the next middleware in the chain or skip to the error handler.
   */
  static middleware (error, req, res, _next) {
    res.status(500).send('TODO ERROR HANDLING' + error.stack);
    // let level = 'info'
    // if (!(error instanceof HttpResponseError)) {
    //   error = HttpResponseError.fromError(error)
    //   level = 'error'
    // }
    // logger[level](`Route '${req.originalUrl}' responds with status code '${error.statusCode}' and public message '${error.publicMessage}' instance '${error.instance}'${level !== 'error' ? error.privateMessage : '\n' + error.details.join('\n')}`)
    // error.writeToResponse(res)
  }

  constructor (publicMessage, statusCode, { privateMessage, cause } = {}) {
    super()
    this.name = HttpResponseError.name

    this.publicMessage = publicMessage
    if (!publicMessage) {
      throw new Error('Argument \'publicMessage\' is required.')
    }

    this.statusCode = statusCode
    if (!statusCode) {
      throw new Error('Argument \'statusCode\' is required.')
    }

    this.cause = cause
    this.privateMessage = privateMessage

    this.instance = Math.floor(Math.random() * 0xFFFFFFFF).toString(16).padStart(8, '0')
  }

  get details () {
    const details = []

    details.push(this.stack)

    if (this.cause) {
      details.push(this.cause.stack)
    }

    if (this.cause && this.cause.response && this.cause.response.data) {
      const data = this.cause.response.data
      if (typeof data === 'object') {
        details.push(JSON.stringify(data, null, 4))
      } else {
        details.push(data)
      }
    }

    if (this.cause && this.cause.validationErrors) {
      details.push(this.cause.validationErrors)
    }

    return details
  }

  toHTML () {
    return htmlTemplateString.replace('%ERROR%', JSON.stringify(this.toJSON()))
  }

  get message () {
    return this.privateMessage
  }

  toJSON () {
    const model = {
      publicMessage: this.publicMessage,
      statusCode: this.statusCode,
      instance: this.instance
    }

    if (process.env.NODE_ENV !== 'production') {
      model.privateMessage = this.privateMessage
      model.details = this.details
    }

    return model
  }

  writeToResponse (res) {
    const req = res.req

    if (req.accepts('text/html')) {
      res.status(this.statusCode).send(this.toHTML())
    } else {
      res.status(this.statusCode).json(this.toJSON())
    }
  }
}

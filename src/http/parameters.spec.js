import { describe, expect, it } from '@jest/globals'
import { requestParameter } from './parameters'

describe('requestParameter()', () => {
  it('throws when parameter is present multiple times', () => {
    const mockRequestparameters = {
      foo: ['Hello', 'World']
    }

    expect(() => {
      requestParameter(mockRequestparameters, 'foo')
    }).toThrow('Query parameter \'foo\' must not occur multiple times')
  })

  it('throws when parameter is empty', () => {
    const mockRequestparameters = {
      foo: ''
    }

    expect(() => {
      requestParameter(mockRequestparameters, 'foo')
    }).toThrow('Query parameter \'foo\' is required')
  })

  it('throws when parameter is missing', () => {
    const mockRequestparameters = {}

    expect(() => {
      requestParameter(mockRequestparameters, 'foo')
    }).toThrow('Query parameter \'foo\' is required')
  })

  it('returns value when parameter is present', () => {
    const mockRequestparameters = {
      foo: 'Hello'
    }

    const value = requestParameter(mockRequestparameters, 'foo')
    expect(value).toBe('Hello')
  })

  it('codes value as URI component', () => {
    const mockRequestparameters = {
      foo: 'Hello%20World'
    }

    const value = requestParameter(mockRequestparameters, 'foo')
    expect(value).toBe('Hello World')
  })
})
/**
 * Validates a query parameter (i.e. ?foo=bar) that must occur excatly once or not at all.
 * When the parameter is provided multiple times an error is thrown as per spec.
 * When the parameter is required and empty or missing an error is thrown
 * When the parameter is not required and empty or missing null is returned.
 * The value of the parameter is decoded as a URI component.
 * @param queryOrBody {Object} Either req.query or req.body.
 * @param parameterName {string} The name of the parameter that is expected.
 * @param required? {boolean} Whether the parameter is required. Defaults to `true`
 * @param acceptedValues? {string[]} Array of accepted string values. Useful in case of enum-likes.
 * @return {string|null} The value of the parameter. Never empty, only null when `required=false`
 * @throws If the parameter does not occur, occurs multiple times or is empty.
 * @see https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-3.2-6
 */
export function requestParameter(queryOrBody, parameterName, {required = true, acceptedValues } = {}) {
  const parameterValue = queryOrBody[parameterName]
  if(!parameterValue) {
    if(required === true) {
      throw new Error(`Query parameter '${parameterName}' is required`);
    } else {
      return null;
    }
  }
  if(Array.isArray(parameterValue)) {
    throw new Error(`Query parameter '${parameterName}' must not occur multiple times`);
  }

  const value = decodeURIComponent(parameterValue)

  if(acceptedValues && !acceptedValues.includes(value)) {
    throw new Error(`Query parameter '${parameterName}' must be any of [${acceptedValues.join(', ')}]`)
  }

  return value;
}

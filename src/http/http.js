import { HttpResponseError } from './error'

/**
 * Parses the value of the Authorization header. Guarantees to either return a valid value or throw an HttpResponseError.
 * @param authorizationHeader The value of the authorization header (e.g. `req.header(Authorization)`).
 * @return {{username: string, password: string}}
 * @throws {HttpResponseError} if anything is wrong.
 */
export function parseAuthorizationHeader (authorizationHeader) {
  const type = authorizationHeader === null ? null : typeof authorizationHeader
  if (type !== 'string') {
    throw new Error(`Authorization header must be of type 'string' but was '${type}'`);
  }

  if (authorizationHeader.trim() === '') {
    throw HttpResponseError.fromScratch('Forbidden', 401, { privateMessage: `Authorization header must not be empty but was '${authorizationHeader}'` })
  }

  const authorizationHeaderParts = authorizationHeader.split(' ')
  if (authorizationHeaderParts.length !== 2) {
    throw new Error(`Authorization header '${authorizationHeader}' is invalid`);
  }

  const authorizationScheme = authorizationHeaderParts[0]
  if (authorizationScheme !== 'Basic') {
    throw new Error(`Require 'Basic' Authorization scheme but was '${authorizationScheme}'`)
  }

  const authorizationCredentials = Buffer.from(authorizationHeaderParts[1], 'base64').toString()
  const separatorIndex = authorizationCredentials.indexOf(':')
  if (separatorIndex === -1) {
    throw new Error('Authorization header credentials must contain a separator \':\'')
  }

  const username = authorizationCredentials.substring(0, separatorIndex)
  const password = authorizationCredentials.substring(separatorIndex + 1, authorizationCredentials.length)
  if (!username || !password) {
    throw new Error('Authorization header username and password must not be empty')
  }

  return { username, password }
}

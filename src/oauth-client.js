import mongoose from 'mongoose'
import { logger } from './logger'
import { randomBytes } from 'crypto'
import { arrayValidator, notBlankStringValidator, validationSchemaObject } from './models/validator'

const schema = new mongoose.Schema({
  // _id: { type: String, required: true, immutable: true, default: randomBase64String.bind(global, 10) },

  name: { type: String, required: true },

  // TODO: is this supposed to be hashed too???
  secret: { type: String, required: true, default: randomBase64String.bind(global, 50) },

  redirectUris: {
    type: [mongoose.SchemaTypes.String],
    required: true,
    validate: validationSchemaObject(arrayValidator([notBlankStringValidator()]))
  }
})

/**
 * Client ID does not need to be cryptographically strong because it's not a secret.
 * The OAuth spec does require this not to be chosen by the client themselves and it must be unique.
 * The ObjectID satisfies these requirements, but we will format it in Base64 to remain consistent with the rest of the
 * tokens and secrets being generated.
 */
schema.virtual('clientId').get(function () {
  return convertHexStringToBase64String(this._id.toString())
})

/**
 * Creation date can be extracted from the ObjectId.
 */
schema.virtual('createdAt').get(function () {
  return this._id.getTimestamp()
})

/**
 * @typedef OAuthClient
 * Retrieves a client from the database by means of its client id.
 * Note that the client id is _not_ the same as the MongoDB ObjectId.
 * @param clientId {string} The client id of the oauth client to retrieve.
 * @param clientSecret {string} The client secret of the oauth client to retrieve.
 * @return {Promise<OAuthClient|null>}
 */
schema.statics.findByClientIdAndSecret = async function (clientId, clientSecret) {
  const client = await this.findByClientId(clientId)
  if (!client) {
    return null
  }

  if (client.secret !== clientSecret) {
    logger.debug(`Client with id '${clientId}' exists, but the client secret is incorrect.`)
    return null
  }

  return client
}

schema.statics.findByClientId = async function (clientId) {
  const client = await this.findOne({ clientId })
  if (!client) {
    logger.debug(`Client with id '${clientId}' does not exist.`)
    return null
  }
  return client
}

/**
 * Generates a crypto grade random bytestring and returns it as a base64 string.
 * @param stringLength {number} The number of random bytes to generate. Note that the returned string will be 33% larger due
 * to base64 encoding expansion.
 * @return {string} The base64 version of the bytes generated.
 */
function randomBase64String (stringLength = 24) {
  return randomBytes(stringLength / 2).toString('hex')
}

/**
 * Covnerts a hexadecimal string into a base64 string (i.e. `7f3cd5` -> `fzzV`)
 * @param hexString The hexadecimal string used as input.
 * @return {string}
 */
function convertHexStringToBase64String (hexString) {
  const numbers = hexString.match(/[0-9a-f]{1,2}/g).map(s => Number.parseInt(s, 16))
  return Buffer.from(new Int8Array(numbers)).toString('base64')
}

/**
 * @deprecated To be replaced with {@see client.model:Client}
 * @type {Model<Document>}
 */
export const OAuthClient = mongoose.model('oauth_client', schema)

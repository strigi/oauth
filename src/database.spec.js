import { describe, expect, it } from '@jest/globals'
import { server, setupMemoryMongo } from './memory-mongo'
import mongoose from 'mongoose'
import { connect, disconnect } from './database'

describe('database', () => {
  setupMemoryMongo({ mongooseConnection: false })

  it('can connect to database', async () => {
    expect(mongoose.connection.readyState).toBe(0)
    await connect(await server.getUri())
    expect(mongoose.connection.readyState).toBe(1)
  })

  it('can disconnect from database', async () => {
    await connect(await server.getUri())
    expect(mongoose.connection.readyState).toBe(1)
    await disconnect()
    expect(mongoose.connection.readyState).toBe(0)
  })
})

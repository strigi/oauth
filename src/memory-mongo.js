// TODO: find a way to use the same port every time (problem; concurent tests)
// this would allow much easier debugging because the connection to the in-memory mongo from mongosh or compass would always
// be the same, thus you can store it as a favourite and not have to search the logs for the right connection URI every run

import { afterAll, beforeAll, beforeEach, expect } from '@jest/globals'
import { MongoMemoryServer } from 'mongodb-memory-server-core'
import mongoose from 'mongoose'
import { logger } from './logger'
import * as path from 'path'

/**
 * The instance of {@link MongoMemoryServer} to use.
 * This is safe to store on module scope, because of JavaScript's single thread model.
 * It is also exposed (though not protected against invalid modification) to allow useful stuff to be retrieved
 * in case a unit test has a special need (see https://github.com/nodkz/mongodb-memory-server#usage)
 * When not connected this will be `null`.
 * @type {MongoMemoryServer}
 */
export let server = null

/**
 * Creates a new instance of {@link MongoMemoryServer} and connects mongoose to it.
 * This can be used in a {@link beforeEach} or {@link beforeAll} for instance.
 * The server is downloaded on the fly into `node_modules/.cache` so make sure the timeout is high enough (say about 30 or so seconds)
 * This is only the first time (after an `npm install`) from then on it'll be super fast.
 * @param mongoDbVersion? {string} The version of MongoDB to use. You have to be a little careful here because it must
 * be a version available for (all) your platform(s) here: https://www.mongodb.com/download-center/community/releases/archive
 * @param mongooseConnection? {boolean} True if you want to connect mongoose to the database. Defaults to true.
 * @return {Promise<MongoMemoryServer>} The server instance already connected started. Don't forget to `await`!
 */
export async function startMemoryMongo ({ mongoDbVersion = '4.2.10', mongooseConnection = true } = {}) {
  logger.info(`Starting in-memory mongo '${mongoDbVersion}' (the first run, this may take a minute, please be patient.)`);

  const dbNameBasedOnFilename = path.basename(expect.getState().testPath).replace(/\.js$/, '').replace(/\./g, '-');
  server = new MongoMemoryServer({ binary: { version: mongoDbVersion }, autoStart: false, instance: { dbName: dbNameBasedOnFilename } })
  await server.start()

  if (mongooseConnection) {
    await connectToMongoose(await server.getUri())
  }

  return server;
}

/**
 * Disconnects the existing instance of {@link MongoMemoryServer} from mongoose and closes the server.
 * This can e ued in a {@link afterEach} or {@link afterAll} for instance.
 * @return {Promise<void>} Nothing useful is returned, but be sure to `await` as it is async.
 */
export async function stopMemoryMongo () {
  if (isConnectedToMongoose()) {
    await mongoose.disconnect()
  }

  if (!server) {
    throw new Error('Your memory mongo is not initialized. Maybe you have a problem with the beforeEach or beforeAll setup if your test.')
  }
  await server.stop()
  server = null
}

// /**
//  * Deletes _all_ collections from the database currently connected to with Mongoose.
//  * It uses the reference mongoose keeps to mongo driver directly. Deleting collections this way is very fast.
//  * This can be used in a {@link beforeEach} or {@link afterEach} for instance.
//  * @return {Promise<void>} Nothing but don't forget to `await`
//  * @deprecated
//  */
// async function deleteAllCollections () {
//   if (!isConnectedToMongoose()) {
//     return
//   }
//
//   const collections = await mongoose.connection.db.collections()
//   const collectionNames = collections.map(c => c.collectionName);
//   logger.info(`Deleting all collections [${collectionNames.join(', ')}] from database '${await server.getUri()}'`);
//   for (const collection of collections) {
//     await collection.drop()
//   }
// }

async function dropDatabase() {
  if (isConnectedToMongoose()) {
    logger.info(`Dropping database '${await server.getDbName()}'`);
    await mongoose.connection.dropDatabase();
  }
}

/**
 * Convenient do-all setup for a typical unit-testing scenario.
 * It will setup the test with a {@link beforeAll} to {@link startMemoryMongo}, a {@link afterAll} to
 * {@link stopMemoryMongo} and a {@link dropDatabase}.
 * A one stop shop to put into a `describe()`. Note that any additional `beforeEach` etc... is executed in order of
 * declaration within the `describe()`, so you can still control the order of all fixtures.
 * @param timeout? {number} The timeout for the beforeAll. This is high because the first run it takes a while to download.
 * @param mongoDbVersion? {string} The version of MongoDB to use, see {@link startMemoryMongo}.
 * @param mongooseConnection? {boolean} True if a connection to mongoose needs to be made, see {@link startMemoryMongo}.
 * @see startMemoryMongo
 * @see stopMemoryMongo
 * @see dropDatabase
 */
export function setupMemoryMongo ({ timeout = 60 * 1000, mongoDbVersion, mongooseConnection } = {}) {
  beforeAll(async () => {
    await startMemoryMongo({ mongoDbVersion, mongooseConnection })
  }, timeout)

  beforeEach(dropDatabase)

  afterAll(stopMemoryMongo)
}

/**
 * Helper function to determine in a readable way if the connection to mongoose is active or not.
 * @return {boolean} True if connected (e.g. readyState is 1.
 */
function isConnectedToMongoose () {
  return mongoose.connection?.readyState === 1
}

/**
 * Connects Mongoose using the given mongodb:// connection string.
 * @param uri Mongodb connection URI string (i.e. mongodb://myUsername:myPassword@myServer:27017/myDatabase?myOptions)
 * @return {Promise<void>} Nothing but don't forget to `await`.
 */
async function connectToMongoose (uri) {
  logger.info(`Connecting to Mongoose using in-memory mongo uri '${uri}'`);
  await mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
  })
}

import { HttpResponseError } from './http/error.js'

export function methodNotAllowed (req, res) {
  throw HttpResponseError.fromScratch('Method Not Allowed', 405)
}

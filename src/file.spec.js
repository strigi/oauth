import { describe, expect, it } from '@jest/globals'
import url from 'url'
import path from 'path'
import { file } from './file'

describe('dirname', () => {
  const f = file(import.meta.url)
  const __dirname = path.dirname(url.fileURLToPath(import.meta.url))

  it('file() returns object with interesting methods and properties', () => {
    expect(f.join).toBeInstanceOf(Function)
    expect(f.dirname).toBeDefined()
  })

  it('dirname returns the absolute dirname', () => {
    expect(f.dirname).toBe(__dirname)
  })

  it('dirname does not have trailing /', () => {
    expect(f.dirname).not.toMatch(/\/$/)
  })

  it('join() appends path segments to current file location', () => {
    expect(f.join('hello', 'world.txt')).toBe(__dirname + `${path.sep}hello${path.sep}world.txt`)
  })

  it('join() cleans up .. and . references', () => {
    expect(f.join('hello', '..', './world.txt')).toBe(__dirname + `${path.sep}world.txt`)
  })

  it('join() removes unnecessary / separators', () => {
    expect(f.join('hello', '//awesome//', '/world.txt')).toBe(__dirname + `${path.sep}hello${path.sep}awesome${path.sep}world.txt`)
  })
})

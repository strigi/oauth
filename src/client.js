import express from 'express'
import session from 'express-session'
import axios from 'axios'

(async () => {
  const app = express()

  app.use(session({ secret: 'blah' }))

  app.get('/', (req, res) => {
    res.send(`
            <!DOCTYPE html>
            <html lang="en">
                <head>
                    <title>Welcome</title>
                    <meta charset="UTF-8"/>
                </head>            
            </html>
            <body>
                <h1>Welcome ${req.session.user ? req.session.user.firstName : ''}</h1>
                ${
                  !req.session.user
                  ? '<p>Welcome to my app. Sign in using:</p><ul><li><a href="/authenticate/google">Google</a></li><li><a href="/authenticate/eothel">Eothel</a></li></ul>'
                  : 'Already logged on: ' + JSON.stringify(req.session.user) + '<a href="/logout">logout</a>'
                }
            </body>            
        `)
  })

  app.get('/logout', (req, res) => {
    delete req.session.user
    res.redirect('/')
  })

  const router = express.Router()

  router.get('/authenticate/:provider', (req, res) => {
    const { provider } = req.params
    switch (provider) {
      case 'google':
        res.redirect('https://accounts.google.com/o/oauth2/v2/auth?response_type=code&redirect_uri=http://localhost:3001/callback/google&client_id=802640105824-1nvftmbtgqp6se7u005278tqheusbutl.apps.googleusercontent.com&scope=openid+profile+email')
        break
      case 'eothel':
        res.redirect('http://localhost:3000/auth?redirect_uri=http://localhost:3001/callback/eothel&client_id=cmc')
        break
      default:
        throw new Error(`Unsupported authentication provider '${provider}'`)
    }
  })

  router.get('/callback/:provider', async (req, res, next) => {
    try {
      const { provider } = req.params
      switch (provider) {
        case 'google': {
          const { code } = req.query
          const response = await axios.post('https://oauth2.googleapis.com/token', {
            client_id: '802640105824-1nvftmbtgqp6se7u005278tqheusbutl.apps.googleusercontent.com',
            client_secret: 'o5_3qUIjPsdD6e0BnE85XeY8',
            code,
            redirect_uri: 'http://localhost:3001/callback/google',
            grant_type: 'authorization_code'
          })
          const jwt = response.data.id_token
          const userInfo = jwt.split('.')[1]
          const user = JSON.parse(Buffer.from(userInfo, 'base64').toString())
          req.session.user = {
            username: user.email,
            email: user.email,
            firstName: user.given_name,
            lastName: user.family_name,
            locale: user.locale.split('-')[0]
          }
          break
        }
        case 'eothel': {
          const { code } = req.query
          const tokenResponse = await axios.post('http://localhost:3000/token', {
            client_id: 'cmc',
            client_secret: 'ons geheimpje',
            redirect_uri: 'http://localhost:3001/callback/eothel',
            code

          }, {
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            }
          })
          const token = tokenResponse.data.access_token
          const userInfoResponse = await axios.get(`http://localhost:3000/userinfo?token=${token}`, {
            headers: {
              Accept: 'application/json'
            }
          })
          req.session.user = userInfoResponse.data
          break
        }
        default: {
          throw new Error(`Unsupported authentication provider '${provider}'`)
        }
      }
      res.redirect('/')
    } catch (error) {
      next(error)
    }
  })

  router.use((error, req, res, next) => {
    const instance = Math.round((Math.random() * 0xFFFFFFFF)).toString(16).padStart(8, '0').toUpperCase()
    const errorDetails = `${error.message} ${error.response && JSON.stringify(error.response.data)} ${error.stack}`
    console.log(`Error: '${instance}, '${errorDetails}`)
    const responseBody = `Error: '${instance}'. '${process.env.NODE_ENV !== 'production' && `Details: ${errorDetails}`}'`
    res.status(500).type('text/plain').send(responseBody)
  })

  app.use(router)

  app.listen(3001, () => {
    console.log('Ready')
  })
})()

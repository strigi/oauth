import express from 'express'
import { methodNotAllowed } from '../method-not-allowed-handler'
import { parseAuthorizationHeader } from '../http/http'
import { Client } from '../models/client/client.model'
import { requestParameter } from '../http/parameters'
import { User } from '../models/user/user.model'

export const router = express.Router()

// DONE: Spec: MUST use only post
// DONE: Spec: empty query parameters must be ignored (query parameters are allowed even if this is POST)
// DONE: Spec: unrecognized query paramters must be ignored
// DONE: Spec: query parameters must only occur once
// TODO: Spec: client authentication RECOMMENDED Basic Authorization header
// TODO: Spec: client authentication MAY support form POST (NOT RECOMMENDED) MUST NOT be passed using query params
// DONE: Spec: support ONLY urlencoded
// TODO: Spec omitted scope -> defaults (use all the scopes from the client?)
// TODO: Spec: access token scopes https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#name-access-token-scope
// TODO: Spec success response token_type access_token (JSON) https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#name-successful-response
// TODO: Spec: expires_in https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#name-successful-response
// TODO: Spec response scopes if different from client
// TODO: Spec: implement error response https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#name-error-response-2
// DONE: Spec: Add Cache-Control and Pragma headers as per spec https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-5.1-4 (is this needed for the /auth endpoint or only the /token?)
// TODO: Spec: implement refresh tokens (optional)
// TODO: Spec: MUST code_challenge PKCE (chapter 4)
// TODO: Spec: redirect_uri optional if it was not passed in /auth (e.g. single registered URI for client
// TODO: Spec: client credentials grant
// TODO: Spec: code already consumed? Revoke all access tokens
router.route('/').post(express.urlencoded({ extended: true }), async (req, res) => {
  // Authenticate client
  const { username: clientId, password: clientSecret } = parseAuthorizationHeader(req.get('Authorization'))
  const client = await Client.findByIdAndSecret(clientId, clientSecret)
  if (!client) {
    throw new Error(`Client '${clientId}' authentication failed`)
  }

  // Spec requires this to be fixed value
  requestParameter(req.body, 'grant_type', { acceptedValues: ['authorization_code'] })

  // Retrieve and validate the authorization code
  const codeId = requestParameter(req.body, 'code')
  const user = await User.findByCode(codeId)
  if (!user) {
    throw new Error(`No user found for authorization code '${codeId}'`)
  }
  const code = user.codeById(codeId)

  // Retrieve and validate redirect_uri
  // TODO duplicated code with findByIdAndRedirectUri()
  let redirectUri = requestParameter(req.body, 'redirect_uri', { required: false })
  if(redirectUri === null) {
    if (client.redirectUris.length !== 1) {
      throw new Error('Ambiguous redirect_uri');
    }
    redirectUri = client.redirectUris[0]
  }
  if(code.redirectUri !== redirectUri) {
    throw new Error(`Code '${codeId}' does not expect redirect_uri '${redirectUri}'`);
  }


  // TODO: code verifier

  const token = code.generateToken([...client.scopes])
  await user.save();

  res.set('Pragma', 'no-cache')
  res.set('Cache-Control', 'no-store')
  res.json({
    access_token: token._id,
    token_type: 'Bearer',
    expires_in: token.expiresIn
  })

}).all(methodNotAllowed)

let user = null

/**
 * Attempts to authenticate the username/password combination.
 * @param username {string}
 * @param password {string}
 * @return {Promise<string|boolean|Error>} Resolves false if username/password is incorrect or the redirect_uri to callback containing an authorization code. On error it rejects with an Error object.
 */
export async function authenticate (username, password) {
  // if(!username || !password) {
  //     throw new Error('Arguments username and password are required');
  // }

  const response = await fetch(location.href, {
    method: 'post',
    body: JSON.stringify({
      username,
      password,
      access_token: user && user.token
    }),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  })

  if (response.status === 401) {
    return false
  }

  if (response.status !== 200) {
    const data = await response.json()
    throw new Error(`Unexpected response: ${response.status} ${data.message}`)
  }

  // if(user) {
  //     document.cookie = `token=${user.token};expires=` + new Date(Date.now() + 3600 * 1000).toDateString();
  //     document.cookie = `kevin=${user.token};expires=` + new Date(Date.now() + 3600 * 1000).toDateString();
  // }

  return response.headers.get('Location')
}

export async function fetchCurrentUser () {
  if (user !== null) {
    return user
  }

  const ACCESS_TOKEN_NAME = 'token'

  console.log('Searching for access token')

  // Check for authorizationCode in the query string
  const searchParams = new URLSearchParams(location.search)

  let token = searchParams.get(ACCESS_TOKEN_NAME)
  if (token) {
    console.log('Found access token in query string! Using it.')
  } else {
    console.log('Query string does not contain access token, continuing the search!')
    const cookies = document.cookie.split(';').map(cookie => {
      const [name, value] = cookie.split('=')
      return { name: name.trim(), value: decodeURIComponent(value) }
    })
    const accessTokenCookie = cookies.find(c => c.name === ACCESS_TOKEN_NAME)
    if (!accessTokenCookie) {
      console.log('Cookies do not contain access token, giving up.')
      return null
    }
    console.log('Cookie containing access token found! Using it.')
    token = accessTokenCookie.value
  }

  console.log(`Retrieving user info with access token '${token}'`)
  const response = await fetch('/userinfo', {
    method: 'get',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    }
  })
  if (response.status !== 200) {
    console.log(`Unable to obtain user info with access token '${token}' because status code was '${response.status}'`)
    return null
  }
  user = await response.json()
  user.token = token
  console.log(`Valid access token ${token} resolved to user '${user.username}'.`)
  return user
}

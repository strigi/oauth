import express from 'express'
import axios from 'axios'

import { OAuthClient } from './oauth-client'
import { logger } from './logger'
import { router as oauthRouter, TOKEN_PATH } from './oauth.router'
import { HttpResponseError } from './http/error'
import { execute } from './database'
import { file } from './file'

const uri = process.env.MONGO_URI || 'mongodb://localhost:27017/eothel'

execute(uri, async () => {
  logger.info('Starting application')

  const app = express()

  // TODO: move under /assets?
  app.use('/vue', express.static(file(import.meta.url).join('../node_modules/vue/dist')))
  app.use('/@fontsource/fira-sans', express.static(file(import.meta.url).join('../node_modules/@fontsource/fira-sans')))
  app.use('/@fontsource/fira-code', express.static(file(import.meta.url).join('../node_modules/@fontsource/fira-code')))
  app.use('/', express.static(file(import.meta.url).join('public')))

  app.use('/', oauthRouter)

  app.get('/auth/callback', async (req, res) => {
    const code = req.query.code
    if (!code) {
      throw HttpResponseError.fromScratch('Query string must contain \'code\' parameter', 400)
    }

    const homeClient = await OAuthClient.findByClientId('eothel')
    const protocolHostAndPort = `${req.protocol}://${req.get('Host')}`
    const tokenResponse = await axios.post(`${protocolHostAndPort}${TOKEN_PATH}`, {
      redirect_uri: homeClient.redirectUris[0],
      code
    }, {
      auth: {
        username: homeClient.clientId,
        password: homeClient.secret
      },
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    if (tokenResponse.status === 400) {
      throw HttpResponseError.fromScratch(`Authorization code '${code}' is invalid`, 400)
    }
    if (tokenResponse.status !== 200) {
      throw new Error(`Unexpected response code '${tokenResponse.status}' for authorization code to access token exchange request. Body: '${tokenResponse.data && tokenResponse.data.toString()}'`)
    }
    const token = tokenResponse.data.access_token

    // TODO: how to deliver the access token to a front-end while using the backchannel (e.g. not simply using a call from the FE directly) (session, flash, cookie, request param???)
    // TODO; websocket! which receives current token upon first connect and pushes updates
    res.cookie('token', token, {
      expires: new Date(0),
      httpOnly: false,
      sameSite: 'strict'
    })
    res.redirect('/')
    // res.status(200).json({ token });
  })

  app.use(HttpResponseError.middleware)

  await new Promise((resolve, reject) => {
    let server
    try {
      const port = 3000
      server = app.listen(port, () => {
        logger.info(`Application listening on port ${port}`)
      })
      server.on('close', resolve)

      process.on('SIGINT', () => {
        server.close()
      })
    } catch (error) {
      reject(error)
    }
  })
  console.log('Graceful shutdown')
})

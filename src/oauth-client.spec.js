import { OAuthClient } from './oauth-client'
import { arrayPropertyTests, nonBlankStringArrayTests } from './models/test.utils'
import { describe, expect, it } from '@jest/globals'

describe('OAuthClient', () => {
  it('assigns a random generated secret', () => {
    const { secret } = new OAuthClient()
    expect(secret).toBeHex(50)
  })

  it('automatically assigns a clientId', () => {
    const { clientId } = new OAuthClient()
    expect(clientId).toBeBase64(16)
  })

  describe('name', () => {
    it('is required', () => {
      const error = new OAuthClient().validateSync('name')
      expect(error.message).toBe('oauth_client validation failed: name: Path `name` is required.')
    })

    it('is cast to a string', () => {
      const client = new OAuthClient({ name: 1507 })
      expect(client.name).toBe('1507')
      expect(client.validateSync('name')).toBeUndefined()
    })
  })

  it('client id is not the same as client secret', () => {
    const { secret, clientId } = new OAuthClient()
    expect(secret).not.toBe(clientId)
  })

  it('generates different secrets', () => {
    const a = new OAuthClient()
    const b = new OAuthClient()
    expect(a.secret === b.secret).toBe(false)
  })

  it('generates different clientId', () => {
    const a = new OAuthClient()
    const b = new OAuthClient()
    expect(a.clientId).not.toBe(b.clientId)
  })

  it('can retrieve the createdAt date', () => {
    const { createdAt } = new OAuthClient()
    expect(createdAt).toBeInstanceOf(Date)

    // If they defer less then one second we're good, since this is based on the MongoDB ObjectId which has a timestamp
    // that has a 1s resolution.
    expect(Date.now() - createdAt).toBeLessThan(1000)
  })

  describe('redirectUris', () => {
    arrayPropertyTests(OAuthClient, 'redirectUris')
    nonBlankStringArrayTests(OAuthClient, 'redirectUris')

    it('casts single value to singleton array', async () => {
      const client = new OAuthClient({ redirectUris: '/single' })
      expect(client.redirectUris).toBeInstanceOf(Array)
      expect([...client.redirectUris]).toEqual(['/single'])
    })
  })
})

expect.extend({
  toBeBase64 (value, length) {
    if (typeof value !== 'string') {
      return result(false)
    }

    if (/^[0-9a-zA-Z+/]+={0,2}$/.exec(value) === null) {
      return result(false)
    }

    if (length && value.length !== length) {
      return result(false)
    }

    return result(true)

    function result (pass) {
      const message = () => `Expected value '${value}' to be a base64 string of ${!length ? 'any ' : ''}length ${length || ''}`
      return { pass, message }
    }
  },

  toBeHex (value, length) {
    try {
      expect(value).toMatch(/^[0-9a-f]+$/)
      expect(typeof value).toBe('string')
      if (length) {
        expect(value).toHaveProperty('length')
      }

      return { pass: true }
    } catch (error) {
      return {
        pass: false,
        message () {
          return `Expected value '${value}' to be a hex string of ${!length ? 'any ' : ''}length ${length || ''}`
        }
      }
    }
  }
})

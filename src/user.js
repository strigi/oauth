import mongoose from 'mongoose'
import { v4 as uuid } from 'uuid'
import { logger } from './logger'
import { OAuthClient } from './oauth-client'

/**
 * The number of milliseconds a authorization code is valid. This is typically very short as they are intended to be
 * immediately consumed by the back-channel. A little leniency is given to allow for network latency.
 * @type {number}
 */
const AUTHORIZATION_CODE_VALIDITY_DURATION = 10 * 1000

/**
 * The number of milliseconds a access token is valid. This should be a compromise between user friendliness and
 * security. It represents the length of a typical user session.
 * @type {number}
 */
const ACCESS_TOKEN_VALIDITY_DURATION = 3600 * 1000

const schema = new mongoose.Schema({
  /**
     * The email of the user.
     */
  // TODO: unique constraint
  email: {
    type: String,
    required: true,
    unique: true
  },

  // TODO hash & salt !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  password: {
    type: String,
    required: true
  },

  firstName: {
    type: String,
    required: true
  },

  lastName: {
    type: String,
    required: true
  },

  // TODO: enum
  locale: {
    type: String,
    required: true
  },

  // TODO: sub-schema with methods like isValid() isExpired() etc...
  accessTokens: [{
    value: { type: String, required: true },
    createdAt: { type: Date, required: true },
    expiresAt: { type: Date, required: true },

    // TODO: is the client necessary here, and what about the redirectURI: do both need to be bound to Access tokens similar to authorization code?
    client: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: OAuthClient.modelName,
      required: true
    }
  }],

  authorizationCodes: [{
    value: { type: String, required: true },
    createdAt: { type: Date, required: true },
    expiresAt: { type: Date, required: true },
    consumedAt: { type: Date, required: false },

    /**
         * The OAuth spec requires an authorization code to be bound to the redirect URI so it can not be
         * used from a different redirectUri than the one it has been created with (e.g. in case a client has multiple
         * redirectUri).
         */
    redirectUri: { type: String, required: true },

    client: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: OAuthClient.modelName,
      required: true
    }
  }]
})

/**
 * The value used as the username to log in with.
 * Currently this is chosen as the email of the user.
 */
schema.virtual('username').get(function () {
  return this.email
})

// TODO: hash password!!
schema.statics.findByUsernameAndPassword = function (username, password) {
  return this.find().byUsernameAndPassword(username, password)
}

schema.query.byUsernameAndPassword = function (username, password) {
  return this.findOne({ email: username, password })
}

schema.query.byAuthorizationCode = function (code) {
  return this.findOne({ 'authorizationCodes.value': code })
}

/**
 * @return {Promise<User>}
 */
schema.query.byAccessToken = function (token) {
  return this.findOne({ 'accessTokens.value': token })
}

schema.statics.findByAuthorizationCode = async function (code) {
  if (!code) {
    throw new Error('Argument \'code\' is required')
  }

  const user = await this.findOne().byAuthorizationCode(code)
  if (!user) {
    logger.debug(`Unable to find user by authorization code '${code}'`)
    return null
  }
  return user
}

/**
 * @param {string} token
 * @return {Promise<User>}
 */
schema.statics.findByValidAccessToken = async function (token) {
  const user = await this.findOne().byAccessToken(token)
  if (!user) {
    logger.debug(`Unable to find user by access token '${token}'`)
    return null
  }

  const access = user.getAccess(token)
  const now = new Date()
  if (now > access.expiresAt) {
    logger.debug(`User '${user.username}' has access token '${token}' but is expired since '${access.expiresAt.toISOString()}' because it is older than ${(ACCESS_TOKEN_VALIDITY_DURATION / 1000).toFixed()} seconds`)
    return null
  }

  logger.debug(`User '${user.username}' has valid access token '${token}' which will expire at '${access.expiresAt.toISOString()}' thus remaining valid for ${((access.expiresAt.getTime() - now) / 1000).toFixed()} more seconds'`)
  return user
}

schema.methods.getAuthorization = function (code) {
  return this.authorizationCodes.find(a => a.value === code)
}

schema.methods.getAccess = function (token) {
  return this.accessTokens.find(a => a.value === token)
}

/**
 * Adds a new authorization code for the user, bound to the given client and redirectUri.
 * When consuming the authorization token, it will only be allowed if the client AND redirectUri are the same.
 * The object will not be saved in the database, you still have to do that yourself.
 * @param client {string} The client to bind to this authorization code
 * @param redirectUri {string} The redirect URI to bind to this authorization code.
 * @return {string} The newly generated code.
 */
schema.methods.addNewAuthorizationCode = function (client, redirectUri) {
  if (!client) {
    throw new Error('Argument \'client\' is required')
  }

  if (!client) {
    throw new Error('Argument \'redirectUri\' is required')
  }

  const code = 'CODE:' + uuid()
  logger.debug(`Generated new authorization code '${code}' for user '${this.username}' bound to client '${client.clientId}' and redirect URI '${redirectUri}'`)
  const now = new Date()
  this.authorizationCodes.push({
    value: code,
    redirectUri: redirectUri,
    createdAt: now,
    expiresAt: new Date(now.getTime() + AUTHORIZATION_CODE_VALIDITY_DURATION),
    client
  })
  return code
}

schema.methods.exchangeAuthorizationCodeForAccessToken = function (code, client, redirectUri) {
  if (!code) {
    throw new Error('Argument \'code\' is required.')
  }

  if (!client || typeof client !== 'object' || !client._id) {
    throw new Error('Argument \'client\' is required and must be a populated Mongoose Document.')
  }

  if (!redirectUri) {
    throw new Error('Argument \'redirectUri\' is required.')
  }

  logger.info(`User '${this.username}' is exchanging authorization code '${code}' for an access token via client '${client.clientId}' for redirect URI '${redirectUri}'. The code will be invalidated.`)

  const authorization = this.getAuthorization(code)
  if (!authorization) {
    logger.debug(`User '${this.username}' does not own authorization code '${code}'`)
    return null
  }

  const now = new Date()
  if (authorization.consumedAt) {
    logger.debug(`User '${this.username}' has authorization code '${code}' but it was already consumed at '${authorization.consumedAt.toISOString()}'.`)
    return null
  }
  authorization.consumedAt = now

  if (now > authorization.expiresAt) {
    logger.debug(`User '${this.username}' has authorization code '${code}' but it is expired since '${authorization.expiresAt.toISOString()}' because it is older than ${(AUTHORIZATION_CODE_VALIDITY_DURATION / 1000).toFixed()} seconds`)
    return null
  }

  // Comparing ObjectId values to avoid having to populate the authorization's client.
  if (authorization.client.toString() !== client._id.toString()) {
    logger.debug(`User '${this.username}' has authorization code '${code}' but it is bound to client '${authorization.client}' instead of the requested '${client.id}'.`)
    return null
  }

  if (authorization.redirectUri !== redirectUri) {
    logger.debug(`User '${this.username}' has authorization code '${code}' but it is bound to redirect uri '${authorization.redirectUri}' instead of the requested '${redirectUri}'.`)
    return null
  }

  logger.debug(`User '${this.username}' has valid authorization code '${code}' bound to client '${client.clientId}' and redirect URI '${redirectUri}' which will expire at '${authorization.expiresAt.toISOString()}' thus remaining valid for ${((authorization.expiresAt.getTime() - now) / 1000).toFixed()} more seconds'`)
  return this.addNewAccessToken(client)
}

schema.methods.addNewAccessToken = function (client) {
  if (!client) {
    throw new Error('Argument \'client\' is required')
  }

  const token = 'TOKEN:' + uuid()
  logger.debug(`Generated new access token '${token}' for user '${this.username}' requested by client '${client.clientId}'`)
  const now = new Date()
  this.accessTokens.push({
    value: token,
    createdAt: now,
    expiresAt: new Date(now.getTime() + ACCESS_TOKEN_VALIDITY_DURATION),
    client
  })
  return token
}

schema.methods.findClientByAuthorizationCode = function (code) {
  const authorization = this.getAuthorization(code)
  if (!authorization) {
    return null
  }

  return OAuthClient.findById(authorization.client)
}

/**
 * Defines a mongoose model that represents a User object.
 * @typedef {username, password, email, firstName, lastName, locale, authorizationCodes, accessTokens} User
 */
export const User = mongoose.model('user', schema)

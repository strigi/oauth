import { logger } from './logger'
import { User } from './user'
import { HttpResponseError } from './http/error'

/**
 * _Express_ middleware to perform an authorization check for a route.
 * Only calls `next` if a valid token is found and can be resolved to a user object, in which case it makes the user
 * available to the next middleware as `req.user`.
 * @example app.get('/hello', authorize(), (req, res) => { res.send(req.user.username) });
 * @return {function(Request, Response, function(Error?)): Promise<undefined>}
 */
export default function authorize () {
  return async (req, res, next) => {
    const token = extractAccessToken(req)
    if (!token) {
      throw HttpResponseError.fromScratch(`Access denied to protected endpoint '${req.originalUrl}' due to no access code present`, 401)
    }

    const user = await User.findByValidAccessToken(token)
    if (!user) {
      throw HttpResponseError.fromScratch(`Access denied to protected endpoint '${req.originalUrl}' due to invalid access token '${token}'`, 401)
    }

    logger.debug(`Access granted to protected endpoint '${req.originalUrl}' for user '${user.username}' using access token '${token}'`)
    req.user = user
    next()
  }
}

/**
 * Attempts to extract an access token from the request.
 * @param req {Request} _Express_ request object.
 * @return {string|null} The access code or null if no access code can be found.
 */
function extractAccessToken (req) {
  let token
  try {
    token = req.query.token
    if (token) {
      logger.debug(`Found access token '${token}' in query parameters for endpoint '${req.originalUrl}'`)
      return token
    }

    const authorizationHeader = req.header('Authorization')
    if (!authorizationHeader) {
      return null
    }

    const authorizationHeaderParts = authorizationHeader.split(' ')
    if (authorizationHeaderParts.length !== 2) {
      return null
    }

    const [type, value] = authorizationHeaderParts
    if (type !== 'Bearer') {
      return null
    }

    token = value
    logger.debug(`Found access token '${token}' on Authorization header for endpoint '${req.originalUrl}'`)
    return token
  } finally {
    if (!token) {
      logger.debug(`Neither query string nor Authorization header contained access token for endpoint '${req.originalUrl}'`)
    }
  }
}

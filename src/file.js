import path from 'path'
import url from 'url'

/**
 * Helper utility to replace the old __dirname (which is no longer available in ESM modules.
 * @param importMetaUrl The module's file URL, accessible from Node as `import.meta.url`.
 * @return {{join(...[string]): string, dir(): string}}
 */
export function file (importMetaUrl) {
  return {
    /**
         * Gets the current file's dirname. This is the new equivalent to __dirname.
         * Useful for relative references to other files.
         * @return {string} The absolute dirname of the currently executing file.
         */
    get dirname () {
      return path.dirname(url.fileURLToPath(importMetaUrl))
    },

    /**
         * Builds a new path from the current file's directory, appending as many path segments as you like.
         * Similar to path.join() but specific for the current executing file's directory.
         * @param paths {...string} all the path segments you wish to add to the current executing files' directory.
         * @return {string} Absolute normalized path (e.g. '..', '.' and '//' removed.
         */
    join (...paths) {
      return path.normalize(path.join(...[this.dirname, ...paths]))
    }
  }
}

import mongoose from 'mongoose'
import { logger } from './logger'

export async function connect (uri) {
  logger.info(`Connecting to MongoDB: ${uri}`)
  await mongoose.connect(uri, {
    useUnifiedTopology: true,
    useCreateIndex: true,
    useNewUrlParser: true
  })
}

export async function disconnect () {
  logger.info('Disconnecting from MongoDB')
  await mongoose.disconnect()
}

export async function execute (uri, fn) {
  try {
    await connect(uri)
    await fn(mongoose)
  } catch (error) {
    logger.error(`Unable to execute transaction due to ${error.stack}`)
  } finally {
    await disconnect()
  }
}

# 🔐 Strigi Eothel's OAuth Server

Implementation of the [OAuth 2.1 specification](https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html).

## TODO:

** ❌ Private key / Secret selection**

Ref:
    - https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-1.3.1-3
    - https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-2.3-2
    - https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-2.3-4

Instead of using a client_secret it's possible to replace it with a key to sign
(I don't understand because we don't want to send a private key to the other side do we???)
Maybe the idea is to sign the data on the client and verify it on the AS/RS for extra security

---

** ❌ Crypto access token **

Ref: https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-1.4-6

May need to ensure the access token is not just a random bytestring, but has some meaningful crypto properties

---

** ❌ Implement refresh tokens (optional) **

Ref: https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-1.5-1

This is not required by the specification, so may be skipped until later and still be compliant.

---

** ✔ Redirections **

Typically 302, but according to the spec ANY other (except 307) way to 'redirect via the user-agent' is allowed
(i.e. using JavaScript `window.location.url`)

** ❌ Authorization Server Metadata **

Ref: https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-1.8-3.1

---

** ❌ Endpoint for service discovery (optional) **
https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-1.8-3.1

** PKCE **

Ref: https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#name-compatibility-with-oauth-20

Required in OAuth 2.1 maar niet in OAuth 2.0 https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#name-compatibility-with-oauth-20

---

** ✔ Redirect URI strict string matching **

Ref: https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#name-compatibility-with-oauth-20

Redirect URIs must do an exact match (not partial like only the domain) in OAuth 2.1 but not in OAuth 2.0

---

** ✔ Client registration **

Ref: https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#name-client-registration

SHALL specify the client type as described in Section 2.1,
provide its client redirect URIs as described in Section 3.1.2, and
include any other information required by the authorization server (e.g., application name, website, description, logo image, the acceptance of legal terms).

So may add additional stuff to the client in the database like description, logo, name, ...

** ❌ Generate client_id **

Ref: https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#section-2.2-3

client_id is not choosable by the client

** ❌ Client authentication

Ref: https://tools.ietf.org/id/draft-ietf-oauth-v2-1-01.html#name-client-password

Must support:
    - ✔ Authorization: Basic czZCaGRSa3F0Mzo3RmpmcDBaQnIxS3REUmJuZlZkbUl3 (passing client_id and client_secret)
    - ❌ Brute force attack protection (slowdown??):

May (optionally but NOT RECOMMENDED) support body in x-www-form-urlencoded: "client_id": "client_secret":
Necessary for other properties too anyways.

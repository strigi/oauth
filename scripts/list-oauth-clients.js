#!/usr/bin/env -S node --es-module-specifier-resolution=node --no-warnings

import { OAuthClient } from '../src/oauth-client'
import { interact } from './interact'
import { execute } from '../src/database'

class Column {
  static stringCasterFn = (maxLength) => {
    return v => {
      if (v === null) {
        return 'null'
      }

      if (v === undefined) {
        return 'undefined'
      }

      return v.toString()
    }
  };

  static dateCaster = (v => {
    if (!(v instanceof Date)) {
      return 'E'
    }
    return v.toISOString()
  });

  static numberCasterFn = (fractionDigits) => v => v.toFixed(fractionDigits);

  constructor (name, casterFn) {
    this.casterFn = casterFn || Column.stringCasterFn()
    this.name = name
    this.length = name.length
  }

  ensureSizeFor (record) {
    this.#ensureLength(this.castValueToStringFor(record).length)
  }

  #ensureLength (length) {
    this.length = Math.max(length, this.length)
  }

  castValueToStringFor (record) {
    return this.casterFn(record[this.name])
  }
}

class Table {
  constructor (columns = []) {
    this.columns = columns.map(c => {
      return c instanceof Column ? c : new Column(c)
    })
    this.records = []
  }

  addRecord (record) {
    this.records.push(record)
    for (const column of this.columns) {
      column.ensureSizeFor(record)
    }
  }

  toString () {
    let string = this.#rowSeparator()

    string += '| '
    this.columns.forEach((c, i, a) => {
      string += c.name.padEnd(c.length) + ' |'
      if (i !== a.length - 1) {
        string += ' '
      } else {
        string += '\n'
      }
    })
    string += this.#rowSeparator()

    // Generate records
    this.records.forEach((r, i, a) => {
      this.columns.forEach((c) => {
        const value = c.castValueToStringFor(r)
        string += `| ${value} `
      })
      string += '|\n'
    })

    string += this.#rowSeparator()

    // Generate footer
    string += `TOTAL: ${this.records.length}\n`

    return string
  }

  #rowSeparator () {
    let string = '+'
    this.columns.forEach((c, i, a) => {
      string += '-'.repeat(c.length + 2) + '+'
    })
    string += '\n'
    return string
  }
}

const uri = await interact('mongodb uri: ', { defaultValue: 'mongodb://localhost:27017/eothel' })
await execute(uri, async () => {
  const clients = await OAuthClient.find().sort('_id')
  const t = new Table(['_id', 'name', 'clientId', 'secret', new Column('createdAt', Column.dateCaster)])
  for (const client of clients) {
    t.addRecord(client)
  }
  console.log(t.toString())
})

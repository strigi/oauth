#!/usr/bin/env -S node --es-module-specifier-resolution=node --no-warnings

import { interact } from './interact'
import { execute } from '../src/database'
import { Client } from '../src/models/client/client.model'

const uri = process.argv[2] || await interact('Database URI', { defaultValue: 'mongodb://localhost/eothel' })
await execute(uri, clients)

async function clients () {
  (await Client.find()).forEach(client => {
    console.log(`- ${client.name.padEnd('20')}${client.id.padEnd('20')}\t${client.secret.padEnd('20')}`)
  })
  console.log(`TOTAL: ${await Client.countDocuments()}`)

  console.log()
  console.log('Create OAuth client (press ^C to exit):')

  const name = await interact(' > Name');
  const redirectUris = await interact(' > Redirect URIs', { converterFn: csv, defaultValue: [`https://${name.toLowerCase()}.be/callback`] });
  const scopes = await interact(' > Scopes', { converterFn: csv, defaultValue: ['profile', 'email']} )

  const client = await new Client({ name, redirectUris, scopes }).save()
  console.log(`Client id = ${client._id}`);
  console.log(`Client secret = ${client.secret}`);
}

function csv(s) {
  return s.split(',').map(s => s.trim());
}
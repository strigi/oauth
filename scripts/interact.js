import readline from 'readline'

export async function interact (question, { converterFn, defaultValue } = {}) {
  while (true) {
    // Build output sentence
    let output = question
    if (defaultValue) {
      output += ` [${defaultValue}]: `
    } else {
      output += ': '
    }

    const answer = await askQuestion(output)
    if (!answer && defaultValue) {
      return defaultValue
    }

    if (!converterFn) {
      return answer
    }

    const result = converterFn(answer)
    if (result) {
      return result
    }
  }
}

function askQuestion (question) {
  const console = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })

  return new Promise((resolve, reject) => {
    console.question(question, answer => {
      console.close()
      resolve(answer.trim() || null)
    })
  })
}

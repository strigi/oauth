#!/usr/bin/env -S node --es-module-specifier-resolution=node --no-warnings

import { User } from '../src/models/user/user.model'
import { interact } from './interact'
import { execute } from '../src/database'

const uri = process.argv[2] || await interact('Database URI', { defaultValue: 'mongodb://localhost/eothel' })
await execute(uri, users)

async function users () {
  (await User.find()).forEach(user => {
    console.log(`- ${user.email} (${user.firstName} ${user.lastName})`)
  })
  console.log(`TOTAL: ${await User.countDocuments()}`)

  console.log()
  console.log('Create user (press ^C to exit):')

  return new User({
    firstName: await interact(' > First name'),
    lastName: await interact(' > Last name'),
    email: await interact(' > Email'),
    password: await interact(' > Password')
  }).save()
}

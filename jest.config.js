/**
 * verbose: faster
 * coverage provider: v8 no syntax errors
 * cobertura: gitlab.com
 * jest-circus and injectGlobals
 */
export default {
  verbose: true,
  testEnvironment: 'node',

  //testRunner: 'jest-circus/runner',
  //injectGlobals: false,

  testMatch: ['**/src/**/*.spec.js'],
  reporters: [
    ['jest-junit', { outputDirectory: 'build' }]
  ],

  collectCoverage: true,
  coverageDirectory: 'build',
  coverageReporters: ['text', 'cobertura'],
  coverageProvider: 'v8',
  collectCoverageFrom: ['**/src/**/*.js'],
  coverageThreshold: {
    global: {
      statements: 40,
      branches: 80,
      functions: 60,
      lines: 40
    }
  }
}

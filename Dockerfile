FROM node:14.16.0

# Run as node instead of root
USER node

# Loation where to put the app
RUN mkdir /home/node/app
WORKDIR /home/node/app

# this is done separately from the source code so Docker can cache this step because it does not change as much as the code (which is a lot faster)
COPY --chown=node:node package*.json /home/node/app/
RUN npm ci --prod

# copy source code to the image
COPY --chown=node:node src /home/node/app/src

# App will run webserver on port 3000
EXPOSE 3000

# Bootstrap command for the container to start the app
CMD node --es-module-specifier-resolution=node src/server
